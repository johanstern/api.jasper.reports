package api.jasper.report.build;

import  api.jasper.report.build.ReportRegistroPDF.HeaderFooter;

import java.awt.Desktop;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.activation.URLDataSource;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.lowagie.text.Cell;
import com.ibm.icu.text.SimpleDateFormat;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Element;
import com.itextpdf.text.Rectangle;


public class ReportRegistroPDF {

	private String path_storage = null;
	private String path_img = null;
	
	public ReportRegistroPDF(){
		
	}	
	
	
	public HttpServletResponse creandoPDF (JSONObject jsonObj,HttpServletResponse response) {
		Document document = new Document(PageSize.LETTER_LANDSCAPE);
		
		
		try {
			DecimalFormat formato= new DecimalFormat("#,###.00");

			//BaseColor myColor = WebColors.getRGBColor("#BE0025");
			BaseColor myColor = WebColors.getRGBColor("#cc0029");
			BaseColor backGris = WebColors.getRGBColor("#B3B3B3");
			BaseColor tColor = WebColors.getRGBColor("#FFFFFF");
			
		
			BaseColor colorLabel = WebColors.getRGBColor("#E5E7E9");

			//Descomentar para guardarlo en memoria.
			
		
			PdfWriter writer = PdfWriter.getInstance(document,response.getOutputStream()); 
		
		
	
			document.open();

			
			//instanciamos la clase HeaderFooter
			HeaderFooter event = new HeaderFooter();
			writer.setPageEvent(event);

			PdfPCell cell;
			
			
		
	
			
			
			
			Font tituloReporte =  new Font(Font.FontFamily.HELVETICA, 15,Font.BOLD, new BaseColor(0,0,0));
			//codec 
			//BaseFont helvetica = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.IDENTITY_H , BaseFont.EMBEDDED);
			
			Font uuid =  new Font(Font.FontFamily.HELVETICA, 8,Font.BOLD, new BaseColor(0,0,0));
			Font nomCampo =  new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD, tColor);
			Font black =  new Font(Font.FontFamily.HELVETICA, 7,Font.NORMAL, new BaseColor(0,0,0));
			Font black2 =  new Font(Font.FontFamily.HELVETICA, 8,Font.NORMAL, new BaseColor(0,0,0));
			Font redN =  new Font(Font.FontFamily.HELVETICA, 9,Font.BOLD, new BaseColor(255,0,0));
			Font blackNegrita =  new Font(Font.FontFamily.HELVETICA, 8,Font.BOLD, new BaseColor(0,0,0));
			

			PdfPTable tablaTitulos = new PdfPTable(3);

			 URLDataSource logo = new URLDataSource(this.getClass().getResource("/static/img/Banco_Santander_Logotipo.png"));
			//URLDataSource source2 = new URLDataSource(this.getClass().getResource("/static/img/SISI.png"));

			 Image img = Image.getInstance(logo.getURL());
			//Image img2 = Image.getInstance(source2.getURL());

			cell = new PdfPCell(img);//celda1-columna0
			cell.setFixedHeight(50f);
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaTitulos.addCell(cell);
			
			System.out.println("JSONModelo ---------------  " + jsonObj);

			Paragraph parag1 = new Paragraph("Mantenimiento de Cuentas Banca Electrónica (Enlace)",blackNegrita);

			Paragraph comb=new Paragraph();
			comb.add(parag1);

			cell = new PdfPCell();
			cell.setFixedHeight(50f);
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaTitulos.addCell(cell);


			cell = new PdfPCell(comb);
			cell.setFixedHeight(50f);
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tablaTitulos.addCell(cell);

			float[] widths0 = { 100f,70f,270f };
			Rectangle r0 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			tablaTitulos.setWidthPercentage(widths0, r0);

			tablaTitulos.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
			//tablaTitulos.setSpacingBefore(5);
			document.add(tablaTitulos);
			
			PdfPTable table2 = new PdfPTable(1);
			
			PdfPTable table1 = new PdfPTable(1);

			cell = new PdfPCell(new Paragraph("1. DATOS GENERALES DE LA EMPRESA: ",blackNegrita));
			cell.setBorder(PdfPCell.BOX);
			cell.setBackgroundColor(backGris);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table1.addCell(cell);
			
			
                 /*
			     *Tabla DATOS GENERALES DE LA EMPRESA HEADERS 1. 
			     *
			     *
			     */
		    
			//String str= jsonObj.getJSONObject("sections").getJSONObject("generalData").get("parameterizationTime").toString();
			//System.out.println(str);
			JSONArray arraySectionsGeneral = new JSONArray();
			JSONArray arraySectionsAccounts = new JSONArray();
			JSONArray arraySectionsAuthorized = new JSONArray();
			JSONArray arraySectionsAccountsN = new JSONArray();
			JSONArray arraySectionsAccountsI = new JSONArray();
			
			String parameterization="";
			int buc=0;
			String contractNumber="";
			
			JSONArray arraySections = jsonObj.getJSONArray("sections");
        	System.out.print("ModeloArray: " +arraySections.toString());
        	
        	
        	for(int i=0;i<arraySections.length();i++){
        		//Se recuperan los programas sociales
        		
        		System.out.println("test"+arraySections.getJSONObject(i));
        		
        		if(arraySections.getJSONObject(i).has("generalData")){
        		
        			JSONObject jsonGeneral = new JSONObject();
        			jsonGeneral.put("generalData",arraySections.getJSONObject(i).getJSONObject("generalData"));
        			arraySectionsGeneral.put(jsonGeneral); 
        			System.out.println("generalData: "+arraySectionsGeneral);
        			parameterization=arraySections.getJSONObject(i).getJSONObject("generalData").getString("parameterizationTime");
        			buc=arraySections.getJSONObject(i).getJSONObject("generalData").getInt("buc");
        			contractNumber=arraySections.getJSONObject(i).getJSONObject("generalData").getString("contractNumber");
        			
        		}
        		
        		if(arraySections.getJSONObject(i).has("accounts")){
        			JSONObject jsonAccounts = new JSONObject();
        			jsonAccounts.put("accounts",arraySections.getJSONObject(i).getJSONArray("accounts"));
        			arraySectionsAccounts.put(jsonAccounts); 
        			System.out.println("accounts: "+arraySectionsAccounts);
        			
        		}
        		if(arraySections.getJSONObject(i).has("authorizedUsers")){
        			JSONObject jsonAuthorized = new JSONObject();
        			jsonAuthorized.put("authorizedUsers",arraySections.getJSONObject(i).getJSONArray("authorizedUsers"));
        			arraySectionsAuthorized.put(jsonAuthorized); 
        			System.out.println("authorizedUsers: "+arraySectionsAuthorized);
        			
        		}
        		
        		if(arraySections.getJSONObject(i).has("accounts")){
        			JSONObject jsonNational = new JSONObject();
        			JSONObject jsonInternational = new JSONObject();
        			for(int j=0;j<arraySections.getJSONObject(i).getJSONArray("accounts").length();j++){
    		    		//System.out.println(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType"));
    		    		if(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType").equals("N")){
    		    			jsonNational.put("national",arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j));
    		    			arraySectionsAccountsN.put(jsonNational);
    		    			System.out.println("national: "+arraySectionsAccountsN);
    		    			//System.out.println("N"+arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j));
    		    		}else if(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType").equals("I")) {
    		    			jsonInternational.put("international", arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j));
    		    			arraySectionsAccountsI.put(jsonInternational);
    		    			System.out.println("international: "+arraySectionsAccountsI);
    		    			//System.out.println("I"+arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j));
    		    		}
    		    	}
        		}
        		
        		
        		
        	}
        	
        	
        	
			
			     PdfPTable tableCompr = new PdfPTable(3);
			     PdfPCell c2;
                 PdfPCell cell2;

			     cell2 = new PdfPCell(new Paragraph("No. CONTRATO ENLACE \n"+contractNumber,black));
			     cell2.setFixedHeight(25f);
			     cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			     tableCompr.addCell(cell2);
			     
			     cell2 = new PdfPCell(new Paragraph("CÓDIGO DE CLIENTE \n"+buc,black));
			     cell2.setFixedHeight(25f);
			     cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			     tableCompr.addCell(cell2);
			     
			     cell2 = new PdfPCell(new Paragraph("NOMBRE, DENOMINACIÓN O RAZÓN SOCIAL (EN LO SUCESIVO EL CLIENTE)\n"+jsonObj.getJSONObject("header").getString("clientName"),black));
			     cell2.setFixedHeight(25f);
			     cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			     tableCompr.addCell(cell2);
			     

			     
			     float[] widthsTableCompr = {80f,80f,250f};
				 Rectangle rTableCompr = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

				 tableCompr.setWidthPercentage(widthsTableCompr, rTableCompr);
				 tableCompr.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
			     
			     c2 = new PdfPCell (tableCompr);
			     c2.setBorder(PdfPCell.BOX);
			     c2.setHorizontalAlignment(Element.ALIGN_CENTER);
				 c2.setPadding(0); 
				 
				 

            cell = new PdfPCell(c2);
			cell.setBorder(PdfPCell.BOX);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(cell);

			float[] widths1 = {470f};
			Rectangle r1 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			table1.setWidthPercentage(widths1, r1);
			table1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
			document.add(table1);
			
			
			/////
			
			 PdfPTable tableDataG2 = new PdfPTable(3);
	         PdfPCell cDataG2;
             PdfPCell cellDataG2;
                
             cDataG2 = new PdfPCell(new Paragraph("NÚMERO Y NOMBRE DE SUCURSAL \n"+jsonObj.getJSONObject("header").getString("officeName")+" "+jsonObj.getJSONObject("header").getInt("officeNumber"),black));
             cDataG2.setFixedHeight(25f);
             cDataG2.setHorizontalAlignment(Element.ALIGN_CENTER);
             tableDataG2.addCell(cDataG2);

            cDataG2 = new PdfPCell(new Paragraph("NOMBRE EJECUTIVO DE CUENTA O PROMOTOR \n"+jsonObj.getJSONObject("header").getString("executiveName"),black));
            cDataG2.setFixedHeight(25f);
            cDataG2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG2.addCell(cDataG2);
            
            cDataG2 = new PdfPCell(new Paragraph("ELABORO FORMATO \n"+jsonObj.getJSONObject("header").getString("owner"),black));
            cDataG2.setFixedHeight(25f);
            cDataG2.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG2.addCell(cDataG2);
		     
		     
		         float[] widthsTableDataG2 = {50f,50f,50f};
			     Rectangle rTableDataG2 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDataG2.setWidthPercentage(widthsTableDataG2, rTableDataG2);
			     tableDataG2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
			     cDataG2 = new PdfPCell (tableDataG2);
			     cDataG2.setBorder(PdfPCell.BOX);
			     cDataG2.setHorizontalAlignment(Element.ALIGN_CENTER);
			     cDataG2.setPadding(0); 

            cell = new PdfPCell(cDataG2);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
			
			 
			//////2. ENTORNO DE CUENTAS:
		    cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
			table2.addCell(cell);
		   
			cell = new PdfPCell(new Paragraph("2. ENTORNO DE CUENTAS: ",blackNegrita));
			cell.setBorder(PdfPCell.BOX);
			cell.setBackgroundColor(backGris);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table2.addCell(cell);
		  
			String text1 = "SE ENTIENDE COMO CUENTA PROPIA, AQUELLA CUYO CÓDIGO DE CLIENTE RELACIONADO ES EL MISMO AL CÓDIGO DE CLIENTE ASOCIADO\n" + 
					"A LA EMPRESA TITULAR DEL CONTRATO ENLACE (11 DÍGITOS).\n" + 
					"SE ENTIENDE POR CUENTA DE TERCEROS, AQUELLA CUYO CÓDIGO DE CLIENTE RELACIONADO ES DIFERENTE AL CÓDIGO DE CLIENTE\n" + 
					"ASOCIADO A LA EMPRESA TITULAR DEL CONTRATO ENLACE (MISMO BANCO: 11 DÍGITOS; OTROS BANCOS: 18 DÍGITOS).";
			
			cell = new PdfPCell(new Paragraph(text1,black2));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table2.addCell(cell);
			
	     	cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
		 	table2.addCell(cell);
			
		 	cell = new PdfPCell(new Paragraph("2.1 TIEMPO DE PARAMETRIZACIÓN: " +parameterization,blackNegrita));
			cell.setBorder(PdfPCell.BOX);
			cell.setBackgroundColor(backGris);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			table2.addCell(cell);
			
			cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
		 	table2.addCell(cell);
		 	
		 	//////
		 	
		 	 PdfPTable tableDataG3 = new PdfPTable(2);
	         PdfPCell cDataG3;
             PdfPCell cellDataG3;
                
             cDataG3 = new PdfPCell(new Paragraph("DESCRIPCIÓN DE COLUMNAS\n" + 
             		"M (MOVIMIENTO) = A (ALTA), B (BAJA), C (CAMBIO)\n" + 
             		"TIPO = 1 (PROPIAS); 2 (TERCEROS MISMO BANCO); 3 (OTROS\n" + 
             		"BANCOS);4 (NUMERO MOVIL SANTANDER) ;\n" + 
             		"5 (NUMERO MOVIL OTROS BANCOS )\n" + 
             		"MONEDA= 1 (MONEDA NACIONAL); 2 (DÓLARES)\n" + 
             		"BANCO = SIGLAS DE LA INSTITUCIÓN FINANCIERA DE DESTINO\n" + 
             		"(VER CATÁLOGO)",black2));
             cDataG3.setFixedHeight(60f);
             cDataG3.setBorder(PdfPCell.NO_BORDER);
             cDataG3.setHorizontalAlignment(Element.ALIGN_LEFT);
             tableDataG3.addCell(cDataG3);

            cDataG3 = new PdfPCell(new Paragraph("DIVISA= EJ. USD, EUR, SEK, CHF, GBP, ETC.\n" + 
            		"CLAVE= SÓLO PARA CUENTAS INTERNACIONALES. ABA.- SÓLO\n" + 
            		"ESTADOS UNIDOS / SWIFT Y CUENTA IBAN SÓLO COMUNIDAD\n" + 
            		"EUROPEA / SWITF RESTO DEL MUNDO",black2));
            cDataG3.setFixedHeight(60f);
            cDataG3.setBorder(PdfPCell.NO_BORDER);
            cDataG3.setHorizontalAlignment(Element.ALIGN_LEFT);
            tableDataG3.addCell(cDataG3);
            
           
		     
		     
		         float[] widthsTableDataG3 = {50f,50f};
			     Rectangle rTableDataG3 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDataG3.setWidthPercentage(widthsTableDataG3, rTableDataG3);
			     tableDataG3.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
			     cDataG3 = new PdfPCell (tableDataG3);
			     cDataG3.setBorder(PdfPCell.NO_BORDER);
			     cDataG3.setHorizontalAlignment(Element.ALIGN_CENTER);
			     cDataG3.setPadding(0); 

            cell = new PdfPCell(cDataG3);
		    cell.setBorder(PdfPCell.NO_BORDER);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		    
		    
		   ////2.2 REGISTRO DE CUENTAS:
		    
		    cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
		 	table2.addCell(cell);
		    
			cell = new PdfPCell(new Paragraph("2.2 REGISTRO DE CUENTAS:",blackNegrita));
			cell.setBorder(PdfPCell.BOX);
			cell.setBackgroundColor(backGris);
			cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
			table2.addCell(cell);
			
			  
		    cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
		 	table2.addCell(cell);
			
			cell = new PdfPCell(new Paragraph("CUENTAS NACIONALES",blackNegrita));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table2.addCell(cell);
			
			/////////tabla nacionales
			
		 	 PdfPTable tableDataG4 = new PdfPTable(6);
	         PdfPCell cDataG4;
             PdfPCell cellDataG4;
                
             cDataG4 = new PdfPCell(new Paragraph("M",black));
             cDataG4.setFixedHeight(30f);
             cDataG4.setBorder(PdfPCell.BOX);
             cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
             tableDataG4.addCell(cDataG4);

            cDataG4 = new PdfPCell(new Paragraph("TIPO",black));
            cDataG4.setFixedHeight(30f);
            cDataG4.setBorder(PdfPCell.BOX);
            cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG4.addCell(cDataG4);
            
            
            cDataG4 = new PdfPCell(new Paragraph("MONEDA",black));
            cDataG4.setFixedHeight(30f);
            cDataG4.setBorder(PdfPCell.BOX);
            cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG4.addCell(cDataG4);

           cDataG4 = new PdfPCell(new Paragraph("NÚMERO DE CUENTA",black2));
           cDataG4.setFixedHeight(30f);
           cDataG4.setBorder(PdfPCell.BOX);
           cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
           tableDataG4.addCell(cDataG4);
		     
           
           cDataG4 = new PdfPCell(new Paragraph("NOMBRE DEL TITULAR DE LA CUENTA O DESCRIPCIÓN",black2));
           cDataG4.setFixedHeight(30f);
           cDataG4.setBorder(PdfPCell.BOX);
           //cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
           cDataG4.setVerticalAlignment(Element.ALIGN_CENTER);
           tableDataG4.addCell(cDataG4);

          cDataG4 = new PdfPCell(new Paragraph("SÓLO PARA TIPO 3 Y 5\n" + 
          		"(OTROS BANCOS)\n" + 
          		"BANCO",black2));
          cDataG4.setFixedHeight(30f);
          cDataG4.setBorder(PdfPCell.BOX);
          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
          tableDataG4.addCell(cDataG4);
          
          for(int i=0;i<arraySections.length();i++){
      		
      		System.out.println("test"+arraySections.getJSONObject(i));
      	
      		
      		if(arraySections.getJSONObject(i).has("accounts")){
      			for(int j=0;j<arraySections.getJSONObject(i).getJSONArray("accounts").length();j++){
  		    		
  		    		if(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType").equals("N")){
  		    			
  		    			System.out.println("Ncdell"+arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j));
  		    			 
  		    			  cDataG4 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType"),black2));
  		    	          cDataG4.setFixedHeight(30f);
  		    	          cDataG4.setBorder(PdfPCell.BOX);
  		    	          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
  		    	          tableDataG4.addCell(cDataG4);
  		    	          
  		    	        cDataG4 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("type"),black2));
		    	          cDataG4.setFixedHeight(30f);
		    	          cDataG4.setBorder(PdfPCell.BOX);
		    	          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
		    	          tableDataG4.addCell(cDataG4);
		    	          
		    	          cDataG4 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("currency"),black2));
  		    	          cDataG4.setFixedHeight(30f);
  		    	          cDataG4.setBorder(PdfPCell.BOX);
  		    	          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
  		    	          tableDataG4.addCell(cDataG4);
  		    	          
  		    	       int num = arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getInt("number");
  		    	          
  		    	        cDataG4 = new PdfPCell(new Paragraph(""+num,black2));
		    	          cDataG4.setFixedHeight(30f);
		    	          cDataG4.setBorder(PdfPCell.BOX);
		    	          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
		    	          tableDataG4.addCell(cDataG4);
		    	          
		    	          cDataG4 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountHolder"),black2));
  		    	          cDataG4.setFixedHeight(30f);
  		    	          cDataG4.setBorder(PdfPCell.BOX);
  		    	          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
  		    	          tableDataG4.addCell(cDataG4);
  		    	          
  		    	        cDataG4 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("bank"),black2));
		    	          cDataG4.setFixedHeight(30f);
		    	          cDataG4.setBorder(PdfPCell.BOX);
		    	          cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
		    	          tableDataG4.addCell(cDataG4);
  		    		}
  		    	}
      		}
      		
   
      		
      	}
          
          
          
          
          
		         float[] widthsTableDataG4 = {10f,10f,15f,30f,60f,30f};
			     Rectangle rTableDataG4 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDataG4.setWidthPercentage(widthsTableDataG4, rTableDataG4);
			     tableDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
			     cDataG4 = new PdfPCell (tableDataG4);
			     cDataG4.setBorder(PdfPCell.NO_BORDER);
			     cDataG4.setHorizontalAlignment(Element.ALIGN_CENTER);
			     cDataG4.setPadding(0); 

            cell = new PdfPCell(cDataG4);
		    cell.setBorder(PdfPCell.NO_BORDER);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		      

        	
		    ////cuentas Internacionales
		    
		    cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
		 	table2.addCell(cell);
			
			cell = new PdfPCell(new Paragraph("CUENTAS INTERNACIONALES",blackNegrita));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table2.addCell(cell);
			
			 PdfPTable tableDataG5 = new PdfPTable(8);
	         PdfPCell cDataG5;
             PdfPCell cellDataG5;
                
             cDataG5 = new PdfPCell(new Paragraph("M",black));
             cDataG5.setFixedHeight(30f);
             cDataG5.setBorder(PdfPCell.BOX);
             cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
             tableDataG5.addCell(cDataG5);

            cDataG5 = new PdfPCell(new Paragraph("TIPO",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            cDataG5 = new PdfPCell(new Paragraph("DIVISA",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            cDataG5 = new PdfPCell(new Paragraph("NÚMERO DE CUENTA",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            cDataG5 = new PdfPCell(new Paragraph("NOMBRE DEL TITULAR DE LA\n" + 
            		"CUENTA (BENEFICIARIO)",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            cDataG5 = new PdfPCell(new Paragraph("BANCO",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            cDataG5 = new PdfPCell(new Paragraph("PAIS O\n" +
            		"CIUDAD",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            cDataG5 = new PdfPCell(new Paragraph("CLAVE",black));
            cDataG5.setFixedHeight(30f);
            cDataG5.setBorder(PdfPCell.BOX);
            cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG5.addCell(cDataG5);
            
            for(int i=0;i<arraySections.length();i++){
          		
          		System.out.println("test"+arraySections.getJSONObject(i));
          	
          		
          		if(arraySections.getJSONObject(i).has("accounts")){
          			for(int j=0;j<arraySections.getJSONObject(i).getJSONArray("accounts").length();j++){
      		    		
      		    		if(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType").equals("I")){
      		    			
      		    			System.out.println("Ncdell"+arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j));
      		    			 
      		    			  cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountType"),black2));
      		    	          cDataG5.setFixedHeight(30f);
      		    	          cDataG5.setBorder(PdfPCell.BOX);
      		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
      		    	          tableDataG5.addCell(cDataG5);
      		    	          
      		    	        cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("type"),black2));
    		    	          cDataG5.setFixedHeight(30f);
    		    	          cDataG5.setBorder(PdfPCell.BOX);
    		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
    		    	          tableDataG5.addCell(cDataG5);
    		    	          
    		    	          cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getJSONObject("bankInfo").getString("badge"),black2));
      		    	          cDataG5.setFixedHeight(30f);
      		    	          cDataG5.setBorder(PdfPCell.BOX);
      		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
      		    	          tableDataG5.addCell(cDataG5);
      		    	          
      		    	       int num = arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getInt("number");
      		    	          
      		    	          cDataG5 = new PdfPCell(new Paragraph(""+num,black2));
    		    	          cDataG5.setFixedHeight(30f);
    		    	          cDataG5.setBorder(PdfPCell.BOX);
    		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
    		    	          tableDataG5.addCell(cDataG5);
    		    	          
    		    	          cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("accountHolder"),black2));
      		    	          cDataG5.setFixedHeight(30f);
      		    	          cDataG5.setBorder(PdfPCell.BOX);
      		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
      		    	          tableDataG5.addCell(cDataG5);
      		    	          
      		    	          cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getString("bank"),black2));
    		    	          cDataG5.setFixedHeight(30f);
    		    	          cDataG5.setBorder(PdfPCell.BOX);
    		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
    		    	          tableDataG5.addCell(cDataG5);
    		    	          
    		    	          cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getJSONObject("bankInfo").getString("country"),black2));
    		    	          cDataG5.setFixedHeight(30f);
    		    	          cDataG5.setBorder(PdfPCell.BOX);
    		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
    		    	          tableDataG5.addCell(cDataG5);
    		    	          
    		    	          cDataG5 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getJSONObject("bankInfo").getString("code"),black2));
    		    	          cDataG5.setFixedHeight(30f);
    		    	          cDataG5.setBorder(PdfPCell.BOX);
    		    	          cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
    		    	          tableDataG5.addCell(cDataG5);
    		    	          
    		    	          System.out.println("RAMO:"+ arraySections.getJSONObject(i).getJSONArray("accounts").getJSONObject(j).getJSONObject("bankInfo").getString("country"));
    		    	          //System.out.println("RAMO:"+jsonObj.getJSONObject("cunidadAdministrativa").getJSONObject("cdependencia").get("ramoPresupuestal").toString()); 		        
      		    		}
      		    	}
          		}
          		
       
          		
          	}
            
            
            
            float[] widthsTableDataG5 = {10f,10f,15f,30f,60f,15f,20f,20f};
		     Rectangle rTableDataG5 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

		     tableDataG5.setWidthPercentage(widthsTableDataG5, rTableDataG5);
		     tableDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
		     cDataG5 = new PdfPCell (tableDataG5);
		     cDataG5.setBorder(PdfPCell.NO_BORDER);
		     cDataG5.setHorizontalAlignment(Element.ALIGN_CENTER);
		     cDataG5.setPadding(0); 

        cell = new PdfPCell(cDataG5);
	    cell.setBorder(PdfPCell.NO_BORDER);
	    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table2.addCell(cell);
			 
		    
	    ///2.3 USUARIOS AUTORIZADOS PARA LA OPERACIÓN DE LA CUENTA:
	    
	    cell = new PdfPCell(new Paragraph("/n",nomCampo));
	    cell.setBorder(PdfPCell.NO_BORDER);
	 	table2.addCell(cell);
		
	 	cell = new PdfPCell(new Paragraph("2.3 USUARIOS AUTORIZADOS PARA LA OPERACIÓN DE LA CUENTA:",blackNegrita));
		cell.setBorder(PdfPCell.BOX);
		cell.setBackgroundColor(backGris);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		table2.addCell(cell);
		
		cell = new PdfPCell(new Paragraph("/n",nomCampo));
	    cell.setBorder(PdfPCell.NO_BORDER);
	 	table2.addCell(cell);
		
		 PdfPTable tableDataG6 = new PdfPTable(7);
         PdfPCell cDataG6;
         PdfPCell cellDataG6;
            
         cDataG6 = new PdfPCell(new Paragraph("CÓDIGO\n" + 
         		"DE\n" + 
         		"CLIENTE",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
		
         
         cDataG6 = new PdfPCell(new Paragraph("NOMBRE DE LA PERSONA\n" + 
         		"AUTORIZADA PARA EL ACCESO\n" + 
         		"AL SISTEMA",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
		
         
         cDataG6 = new PdfPCell(new Paragraph("NÚMERO DE CUENTA PARA\n" + 
         		"OPERAR FONDOS\n" + 
         		"DEINVERSIÓN",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
		
         
         cDataG6 = new PdfPCell(new Paragraph("NÚMERO DE CUENTAS EN\n" + 
         		"LAS QUE TIENE\n" + 
         		"LIMITACIONES LA PERSONA\n" + 
         		"AUTORIZADA",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
		
         
         cDataG6 = new PdfPCell(new Paragraph("NÚMERO\n" + 
         		"DE\n" + 
         		"FACULTAD",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
		
         
         cDataG6 = new PdfPCell(new Paragraph("AGREGAR\n" + 
         		"O\n" + 
         		"ELIMINAR",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
         
         
         cDataG6 = new PdfPCell(new Paragraph("LÍMITE ($)",black));
         cDataG6.setFixedHeight(30f);
         cDataG6.setBorder(PdfPCell.BOX);
         cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
         tableDataG6.addCell(cDataG6);
         
         for(int i=0;i<arraySections.length();i++){
       		
       		System.out.println("test"+arraySections.getJSONObject(i));
       	
       		
       		if(arraySections.getJSONObject(i).has("authorizedUsers")){
       			for(int j=0;j<arraySections.getJSONObject(i).getJSONArray("authorizedUsers").length();j++){
   		    		
   		    		
   		    			
   		       System.out.println("users"+arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).get("accountsAuthorized").toString());
   		    			 
   		      System.out.println(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).get("faculties").toString());  
   		    	
   		      System.out.println(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getJSONArray("faculties").toString());
   		      
   		      String str =arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).get("faculties").toString();
   		     str.replaceAll("\\[", " ").replaceAll("\\]", " ");
   		     str.replace('[', ' ');
   		     str.replace(']', ' ');
   		      System.out.println(str);
   		      
   		    cDataG6 = new PdfPCell(new Paragraph(""+arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getInt("userCode"),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
            
            cDataG6 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getString("accountHolder"),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
            
            cDataG6 = new PdfPCell(new Paragraph(""+arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getInt("accountNumber"),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
            
            cDataG6 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).get("accountsAuthorized").toString(),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
            
            cDataG6 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getJSONArray("faculties").toString(),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
            
            cDataG6 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getString("movement"),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
            
            cDataG6 = new PdfPCell(new Paragraph(arraySections.getJSONObject(i).getJSONArray("authorizedUsers").getJSONObject(j).getString("limit"),black));
            cDataG6.setFixedHeight(30f);
            cDataG6.setBorder(PdfPCell.BOX);
            cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableDataG6.addCell(cDataG6);
   		      
   		      
   		    	}
       		}
       		
    
       		
       	}
         
		
		
         float[] widthsTableDataG6 = {15f,30f,35f,30f,20f,15f,15f};
	     Rectangle rTableDataG6 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

	     tableDataG6.setWidthPercentage(widthsTableDataG6, rTableDataG6);
	     tableDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
     
	     cDataG6 = new PdfPCell (tableDataG6);
	     cDataG6.setBorder(PdfPCell.NO_BORDER);
	     cDataG6.setHorizontalAlignment(Element.ALIGN_CENTER);
	     cDataG6.setPadding(0); 

    cell = new PdfPCell(cDataG6);
    cell.setBorder(PdfPCell.NO_BORDER);
    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    table2.addCell(cell);
		
    
    
    
    ///3. AVISOS LEGALES
    
    
    cell = new PdfPCell(new Paragraph("/n",nomCampo));
    cell.setBorder(PdfPCell.NO_BORDER);
 	table2.addCell(cell);
	
 	cell = new PdfPCell(new Paragraph("3. AVISOS LEGALES",blackNegrita));
	cell.setBorder(PdfPCell.BOX);
	cell.setBackgroundColor(backGris);
	cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
	table2.addCell(cell);
	
	cell = new PdfPCell(new Paragraph("/n",nomCampo));
    cell.setBorder(PdfPCell.NO_BORDER);
 	table2.addCell(cell);
 	
 	
 	String text2 = "EL CLIENTE ESTÁ CONSCIENTE DE LA NATURALEZA DEL USO DE LAS SIGUIENTES FUNCIONALIDADES DEL SISTEMA, EN LOS TÉRMINOS\n" + 
 			"SIGUIENTES:";
	
	cell = new PdfPCell(new Paragraph(text2,black2));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	table2.addCell(cell);
	
	cell = new PdfPCell(new Paragraph("FACULTADES SOBRE CUENTAS",blackNegrita));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	table2.addCell(cell);
	
	
 	String text3 = "DECLARO QUE ES VOLUNTAD DE MI REPRESENTADA AUTORIZAR A LAS PERSONAS QUE SE INDICAN A FIN QUE CUENTEN CON LAS\n" + 
 			"FACULTADES SEÑALADAS EN LA LISTA DE FACULTADES Y PERFILES QUE FORMA PARTE DEL PRESENTE ANEXO, POR LO QUE INSTRUYO A\n" + 
 			"BANCO SANTANDER (MÉXICO), S. A., INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO SANTANDER MÉXICO A ATENDER LAS\n" + 
 			"INSTRUCCIONES QUE EN EL MARCO DE LAS FACULTADES CONFERIDAS POR MÍ EN ESTE ACTO, REALICEN O APAREZCAN REALIZADAS POR\n" + 
 			"LAS PERSONAS QUE AUTORIZO, BAJO LA TOTAL Y EXCLUSIVA RESPONSABILIDAD DE MI MANDANTE.";
	
	cell = new PdfPCell(new Paragraph(text3,black2));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	table2.addCell(cell);
	
	cell = new PdfPCell(new Paragraph("PARAMETRIZACIÓN DE ALTA DE CUENTAS",blackNegrita));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	table2.addCell(cell);
	
	
 	String text4 = "DECLARO QUE ES VOLUNTAD DE MI REPRESENTADA AUTORIZAR A LAS PERSONAS QUE SE INDICAN A FIN QUE CUENTEN CON LAS\n" + 
 			"FACULTADES SEÑALADAS EN LA LISTA DE FACULTADES Y PERFILES QUE FORMA PARTE DEL PRESENTE ANEXO, POR LO QUE INSTRUYO A\n" + 
 			"BANCO SANTANDER (MÉXICO), S.A., INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO SANTANDER MÉXICO A ATENDER LAS\n" + 
 			"INSTRUCCIONES QUE EN EL MARCO DE LAS FACULTADES CONFERIDAS POR MÍ EN ESTE ACTO, REALICEN O APAREZCAN REALIZADAS POR\n" + 
 			"LAS PERSONAS QUE AUTORIZO, BAJO LA TOTAL Y EXCLUSIVA RESPONSABILIDAD DE MI MANDANTE.\n" + 
 			"ADICIONALMENTE, POR ASÍ CONVENIR A MIS INTERESES Y ADVERTIDO DE LOS RIESGOS INHERENTES QUE IMPLICA MI SOLICITUD, SE\n" + 
 			"REALICE EL REGISTRO DE CUENTAS QUE INSTRUYA A ESTA INSTITUCIÓN.";
	
	cell = new PdfPCell(new Paragraph(text4,black2));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	table2.addCell(cell);
	
	cell = new PdfPCell(new Paragraph("USO DE FUNCIONALIDADES DEL SISTEMA",blackNegrita));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	table2.addCell(cell);
	
	
 	String text5 = "MANIFIESTO QUE LEÍ Y CONOZCO LA “LISTA DE FACULTADES Y PERFILES”, Y QUE LA ASIGNACIÓN DE FACULTADES Y PERFILES A LOS\n" + 
 			"USUARIOS CUYO NOMBRE APARECE EN LA COLUMNA SEÑALADA BAJO EL RUBRO “NOMBRE DE LA PERSONA AUTORIZADA PARA ACCESO\n" + 
 			"AL SISTEMA” ASÍ COMO PARA QUE DICHOS USUARIOS CUENTEN CON UN DISPOSITIVO ELECTRÓNICO TOKEN ES DE LA EXCLUSIVA\n" + 
 			"RESPONSABILIDAD DE MI REPRESENTADA\n" + 
 			"EN TAL VIRTUD, EXPRESAMENTE LIBERO A BANCO SANTANDER (MÉXICO), S. A, INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO\n" + 
 			"SANTANDER MÉXICO, AL GRUPO FINANCIERO DEL QUE FORMA PARTE, ASÍ COMO A SUS ENTIDADES FILIALES Y RELACIONADAS, SUS\n" + 
 			"EMPLEADOS Y FUNCIONARIOS, DE CUALQUIER RESPONSABILIDAD DERIVADA O RELACIONADA CON EL OTORGAMIENTO, USO, USO INDEBIDO, USO EN EXCESO, O CUALQUIER FORMA DE EJERCICIO DE LAS FACULTADES OTORGADAS POR MI REPRESENTADA EN TÉRMINOS\n" + 
 			"DE ESTE DOCUMENTO, QUEDANDO POR TANTO MI REPRESENTADA OBLIGADA A SACARLES EN PAZ, A SALVO E INDEMNES DE CUALQUIER\n" + 
 			"QUEJA, DENUNCIA, QUERELLA, DEMANDA, INTERPELACIÓN JUDICIAL, O DE CUALQUIER NATURALEZA QUE SE PRESENTE EN SU CONTRA\n" + 
 			"POR TALES MOTIVOS.";
	
	cell = new PdfPCell(new Paragraph(text5,black2));
	cell.setBorder(PdfPCell.NO_BORDER);
	cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	table2.addCell(cell);
    
	
	///4. CONSENTIMIENTO DEL CLIENTE RESPECTO A LA TOTALIDAD DE LOS DATOS CONTENIDOS EN LA PRESENTE CARÁTULA
	//DE ACTIVACIÓN
    
	    cell = new PdfPCell(new Paragraph("/n",nomCampo));
	    cell.setBorder(PdfPCell.NO_BORDER);
	 	table2.addCell(cell);
		
	 	cell = new PdfPCell(new Paragraph("4. CONSENTIMIENTO DEL CLIENTE RESPECTO A LA TOTALIDAD DE LOS DATOS CONTENIDOS EN LA PRESENTE CARÁTULA DE ACTIVACIÓN",blackNegrita));
		cell.setBorder(PdfPCell.BOX);
		cell.setBackgroundColor(backGris);
		cell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		table2.addCell(cell);
		
		cell = new PdfPCell(new Paragraph("/n",nomCampo));
	    cell.setBorder(PdfPCell.NO_BORDER);
	 	table2.addCell(cell);
	 	
	 	String text6 = "EL CLIENTE DECLARA BAJO PROTESTA DE DECIR VERDAD QUE LOS DATOS DETALLADOS EN ESTE DOCUMENTO SON CIERTOS Y AUTORIZA\n" + 
	 			"A BANCO SANTANDER (MÉXICO), S.A, INSTITUCIÓN DE BANCA MÚLTIPLE, GRUPO FINANCIERO SANTANDER MÉXICO, A QUE LOS\n" + 
	 			"COMPRUEBE A SU ENTERA SATISFACCIÓN.  ASIMISMO, DECLARA EL CLIENTE, PREVIA LECTURA DE LOS DOCUMENTOS RELATIVOS AL\n" + 
	 			"SERVICIO BANCARIO CONTRATADO, QUE RECIBE UNA COPIA DEL CONTRATO RESPECTIVO Y DE LA PRESENTE CARÁTULA DE ACTIVACIÓN,\n" + 
	 			"SUJETÁNDOSE A TODAS Y CADA UNA DE LAS CLÁUSULAS, FIRMANDO EL PRESENTE DOCUMENTO COMO PRUEBA DE SU ENTREGA,\n" + 
	 			"LECTURA Y CONFORMIDAD.  DE IGUAL FORMA, DECLARA HABER LEIDO EL CATALOGO DE PERFILES Y FACULTADES QUE COMO ANEXO\n" + 
	 			"FORMA PARTE DE ESTE DOCUMENTO, CON BASE EN LO CUAL AUTORIZA A LAS PERSONAS REGISTRADAS EN ESTE DOCUMENTO A OPERAR\n" + 
	 			"LAS CUENTAS CON LAS PRERROGATIVAS Y RESTRICCIONES MARCADAS EN EL MISMO.";
		
		cell = new PdfPCell(new Paragraph(text6,black2));
		cell.setBorder(PdfPCell.NO_BORDER);
		cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		table2.addCell(cell);
		
		SimpleDateFormat formateador = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("es_ES"));
				   Date fechaDate = new Date();
				   String fecha = formateador.format(fechaDate);
				   System.out.println(fecha);
			
		   cell = new PdfPCell(new Paragraph("/n",nomCampo));
	       cell.setBorder(PdfPCell.NO_BORDER);
	       table2.addCell(cell);
				   
		  String text7 = "ESTE FORMATO SE FIRMA EN LA CIUDAD DE";
					
			cell = new PdfPCell(new Paragraph(text7+"  "+jsonObj.getJSONObject("header").getString("city")+"  "+fecha,black2));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			table2.addCell(cell);			   
				   
			cell = new PdfPCell(new Paragraph("/n",nomCampo));
		    cell.setBorder(PdfPCell.NO_BORDER);
	        table2.addCell(cell);
			
			cell = new PdfPCell(new Paragraph("EL CLIENTE",blackNegrita));
			cell.setBorder(PdfPCell.NO_BORDER);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table2.addCell(cell);
			
			
			
			 PdfPTable tableDataG7 = new PdfPTable(2);
	         PdfPCell cDataG7;
	         PdfPCell cellDataG7;
	         
	         
	            
	         cDataG7 = new PdfPCell(new Paragraph("NOMBRE(S) Y FIRMA(S)",black));
	         cDataG7.setFixedHeight(120f);
	         cDataG7.setBorder(Rectangle.BOTTOM);
	         cDataG7.setHorizontalAlignment(Element.ALIGN_CENTER);
	         cDataG7.setVerticalAlignment(Element.ALIGN_BOTTOM);
	         tableDataG7.addCell(cDataG7);
	         
	         /*Paragraph p = new Paragraph("This line will be underlined with a dotted line.");
	         DottedLineSeparator dottedline = new DottedLineSeparator();
	         dottedline.setOffset(-2);
	         dottedline.setGap(2f);
	         p.add(dottedline);
	         
	            cell = new PdfPCell(p);
				cell.setBorder(PdfPCell.NO_BORDER);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				table2.addCell(cell);*/
	         
	         cDataG7 = new PdfPCell(new Paragraph("SELLO DE VERIFICACIÓN DE FIRMAS\n"+
	        		 "\n" +
	        		 "\n" + 
	        		 "\n" + 
	        		 "\n" +
	        		 "\n" +
	        		 "\n" +
	        		 "\n" +
	        		 "\n" +
	        		 "\n" +
	        		 "NÚMERO DE FIRMAS______________________________________________\n"+
	        		 "NÚM. DE SUCURSAL______________________________________________\n"+
	        		 "\n" +
	        		 "NOMBRE COLMPLETO DEL SUBDIRECTOR",black));
		         cDataG7.setFixedHeight(120f);
		         cDataG7.setBorder(PdfPCell.BOX);
		         cDataG7.setHorizontalAlignment(Element.ALIGN_CENTER);
		         cDataG7.setRowspan(5);
		         tableDataG7.addCell(cDataG7);
			
	         float[] widthsTableDataG7 = {50f,50f};
		     Rectangle rTableDataG7 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

		     tableDataG7.setWidthPercentage(widthsTableDataG7, rTableDataG7);
		     tableDataG7.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
		     cDataG7 = new PdfPCell (tableDataG7);
		     cDataG7.setBorder(PdfPCell.NO_BORDER);
		     cDataG7.setHorizontalAlignment(Element.ALIGN_CENTER);
		     cDataG7.setPadding(0); 

	    cell = new PdfPCell(cDataG7);
	    cell.setBorder(PdfPCell.NO_BORDER);
	    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table2.addCell(cell);
			
			/*Fin I. Control del Resgistro de Adhesion*/
			
			
			/*Inicio II. Datos generales*/
			//PdfPTable table2 = new PdfPTable(1);

/*			cell = new PdfPCell(new Paragraph("II. Datos generales",nomCampo));
			cell.setBorder(PdfPCell.BOX);
			cell.setBackgroundColor(myColor);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(cell);

                   
                 *Tabla con los datos del apartado 1. 
		         
		
		         PdfPTable tableDG = new PdfPTable(6);
		         PdfPCell cDG;
                 PdfPCell cellDG;

                 cellDG = new PdfPCell(new Paragraph("Orden de Gobierno : ",blackNegrita));
                 cellDG.setFixedHeight(13.33f);
                 cellDG.setBackgroundColor(colorLabel);
                 cellDG.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG.addCell(cellDG);
		     
                 cellDG = new PdfPCell(new Paragraph(jsonObj.has("cordenDeGobierno")?
                		               (!jsonObj.isNull("cordenDeGobierno")?
                		                (jsonObj.getJSONObject("cordenDeGobierno").has("chrDescripcionOrdenGob")?
                		                 jsonObj.getJSONObject("cordenDeGobierno").getString("chrDescripcionOrdenGob"):""):""):"",black));
                 cellDG.setFixedHeight(13.33f);
                 cellDG.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG.addCell(cellDG);
		     
                 cellDG = new PdfPCell(new Paragraph("Entidad Federativa : ",blackNegrita));
                 cellDG.setFixedHeight(13.33f);
                 cellDG.setBackgroundColor(colorLabel);
                 cellDG.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG.addCell(cellDG);
		     
                 cellDG = new PdfPCell(new Paragraph(jsonObj.has("centidadFederativa")?
                		               (!jsonObj.isNull("centidadFederativa")?
                		            	(jsonObj.getJSONObject("centidadFederativa").has("chrDescripcionEntidadFed")?
                		            	 jsonObj.getJSONObject("centidadFederativa").getString("chrDescripcionEntidadFed"):""):""):"",black));
                 cellDG.setFixedHeight(13.33f);
                 cellDG.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG.addCell(cellDG);
                 
                 cellDG = new PdfPCell(new Paragraph("Municipio : ",blackNegrita));
                 cellDG.setFixedHeight(13.33f);
                 cellDG.setBackgroundColor(colorLabel);
                 cellDG.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG.addCell(cellDG);
		         //+jsonObj.getJSONObject("cmunicipio").getString("chrDescripcion")
                 
                 cellDG = new PdfPCell(new Paragraph(jsonObj.has("cmunicipio")?
                		 (!jsonObj.isNull("cmunicipio")?
                		               (jsonObj.getJSONObject("cmunicipio").has("chrDescripcion")?
                		            	jsonObj.getJSONObject("cmunicipio").getString("chrDescripcion"):""):""):"",black));
                 cellDG.setFixedHeight(13.33f);
                 cellDG.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG.addCell(cellDG);
                 
                 float[] widthsTableDG = {60f,66f,60f,111f,66f,112f};
			     Rectangle rTableDG = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDG.setWidthPercentage(widthsTableDG, rTableDG);
			     tableDG.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
		         cDG = new PdfPCell (tableDG);
		         cDG.setBorder(PdfPCell.BOX);
		         cDG.setHorizontalAlignment(Element.ALIGN_CENTER);
		         cDG.setPadding(0); 
			 
			 

            cell = new PdfPCell(cDG);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
                 
                 *//****//*
		    
		     PdfPTable tableDG2 = new PdfPTable(6);
	         PdfPCell cDG2;
             PdfPCell cellDG2;
                 
                 cellDG2 = new PdfPCell(new Paragraph("Ramo presupuestal : ",blackNegrita));
                 cellDG2.setFixedHeight(13.33f);
                 cellDG2.setBackgroundColor(colorLabel);
                 cellDG2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG2.addCell(cellDG2);
                 
                 String ramo = jsonObj.has("cunidadAdministrativa")?
                		       (jsonObj.isNull("cmunicipio")?
                		       (jsonObj.getJSONObject("cunidadAdministrativa").has("cdependencia")?
                		        jsonObj.getJSONObject("cunidadAdministrativa").getJSONObject("cdependencia").get("ramoPresupuestal").toString():""):""):"";
                 
                //System.out.println("RAMO:"+jsonObj.getJSONObject("cunidadAdministrativa").getJSONObject("cdependencia").get("ramoPresupuestal").toString()); 		        
                		        
                 cellDG2 = new PdfPCell(new Paragraph(ramo,black));
                 cellDG2.setFixedHeight(13.33f);
                 cellDG2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG2.addCell(cellDG2);
		     
                 cellDG2 = new PdfPCell(new Paragraph("Dependencia o Entidad : ",blackNegrita));
                 cellDG2.setFixedHeight(13.33f);
                 cellDG2.setBackgroundColor(colorLabel);
                 cellDG2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG2.addCell(cellDG2);
		     
                 cellDG2 = new PdfPCell(new Paragraph(""+jsonObj.get("chrDescripcionDependencia").toString(),black));
                 //System.out.println(jsonObj.getString("chrDescripcionDependencia"));
                 cellDG2.setFixedHeight(13.33f);
                 cellDG2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG2.addCell(cellDG2);
                 
                 cellDG2 = new PdfPCell(new Paragraph("Clave de la Dependencia o Entidad : ",blackNegrita));
                 cellDG2.setFixedHeight(13.33f);
                 cellDG2.setBackgroundColor(colorLabel);
                 cellDG2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG2.addCell(cellDG2);
		     
                 cellDG2 = new PdfPCell(new Paragraph(""+jsonObj.getString("chrCveDependencia"),black));
                 cellDG2.setFixedHeight(13.33f);
                 cellDG2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG2.addCell(cellDG2);
		     
		     
		         float[] widthsTableDG2 = {60f,20f,70f,124f,96f,30f};
			     Rectangle rTableDG2 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDG2.setWidthPercentage(widthsTableDG2, rTableDG2);
			     tableDG2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
			     cDG2 = new PdfPCell (tableDG2);
			     cDG2.setBorder(PdfPCell.BOX);
			     cDG2.setHorizontalAlignment(Element.ALIGN_CENTER);
			     cDG2.setPadding(0); 
			 
			 

            cell = new PdfPCell(cDG2);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		    
		    
		    /////clasificacion de la dependencia OSC OTRO
		    
		    
		    // String valueClas =   jsonObj.get("cscClasFuncDep").toString();
		     
		     String valueClas = jsonObj.has("cscClasFuncDep")?
		    		 (!jsonObj.isNull("cscClasFuncDep")?"  " +  jsonObj.getJSONObject("cscClasFuncDep").getString("chrDescripcion"):""):"";
		    		 
		    		// jsonObj.getJSONObject("cTipoColaborador").getString("chrDescripcion")
		     
		     if(valueClas.equals("null"))
		    	 valueClas ="-";
		     if(valueClas.equals("1"))
		    	 valueClas ="Legislación";
		     if(valueClas.equals("2"))
		    	 valueClas ="Justicia";
		     if(valueClas.equals("3"))
		    	 valueClas ="Coordinación de la Política de Gobierno";
		     if(valueClas.equals("4"))
		    	 valueClas ="Relaciones Exteriores";
		     if(valueClas.equals("5"))
		    	 valueClas ="Asuntos Financieros y Hacendarios";
		     if(valueClas.equals("6"))
		    	 valueClas ="Seguridad Nacional";
		     if(valueClas.equals("7"))
		    	 valueClas ="Asuntos de Orden Público y de Seguridad Interior";
		     if(valueClas.equals("8"))
		    	 valueClas ="Otros Servicios Generales";
		     if(valueClas.equals("9"))
		    	 valueClas ="Protección Ambiental";
		     if(valueClas.equals("10"))
		    	 valueClas ="Vivienda y Servicios a la Comunidad";
		     if(valueClas.equals("11"))
		    	 valueClas ="Salud";
		     if(valueClas.equals("12"))
		    	 valueClas ="Recreación, Cultura y Otras Manifestaciones Sociales";
		     if(valueClas.equals("13"))
		    	 valueClas ="Educación";
		     if(valueClas.equals("14"))
		    	 valueClas ="Protección Social";
		     if(valueClas.equals("15"))
		    	 valueClas ="Otros Asuntos Sociales";
		     if(valueClas.equals("16"))
		    	 valueClas ="Asuntos Económicos, Comerciales y Laborales en General";
		     if(valueClas.equals("17"))
		    	 valueClas ="Agropecuaria, Silvicultura, Pesca y Caza";
		     if(valueClas.equals("18"))
		    	 valueClas ="Combustibles y Energía";
		     if(valueClas.equals("19"))
		    	 valueClas ="Minería, Manufacturas y Construcción";
		     if(valueClas.equals("20"))
		    	 valueClas ="Transporte";
		     if(valueClas.equals("21"))
		    	 valueClas ="Comunicaciones";
		     if(valueClas.equals("22"))
		    	 valueClas ="Turismo";
		     if(valueClas.equals("23"))
		    	 valueClas ="Ciencia, Tecnología e Innovación";
		     if(valueClas.equals("24"))
		    	 valueClas ="Otras Industrias y Otros Asuntos Económicos";
		     if(valueClas.equals("25"))
		    	 valueClas ="Transacciones de la Deuda Pública / Costo Financiero de la Deuda";
		     if(valueClas.equals("26"))
		    	 valueClas ="Transferencias, Participaciones y Aportaciones entre diferentes Niveles y Ordenes de Gobierno";
		     if(valueClas.equals("27"))
		    	 valueClas ="Saneamiento del Sistema Financiero";
		     if(valueClas.equals("28"))
		    	 valueClas ="Adeudos de Ejercicios Fiscales Anteriores";
		    
		     PdfPTable tableDG2clas = new PdfPTable(2);
	         PdfPCell cDG2clas;
             PdfPCell cellDG2clas;
                
            cDG2clas = new PdfPCell(new Paragraph("Clasificación Funcional de la Dependencia (Orden de gobierno Estatal y Municipal): ",blackNegrita));
            cDG2clas.setFixedHeight(13.33f);
            cDG2clas.setBackgroundColor(colorLabel);
            cDG2clas.setHorizontalAlignment(Element.ALIGN_LEFT);
            tableDG2clas.addCell(cDG2clas);

                cellDG2clas = new PdfPCell(new Paragraph(valueClas,black));
                cellDG2clas.setFixedHeight(13.33f);
                cellDG2clas.setHorizontalAlignment(Element.ALIGN_LEFT);
                tableDG2clas.addCell(cellDG2clas);
		     
		     
		         float[] widthsTableDG2clas = {50f,50f};
			     Rectangle rTableDG2clas = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDG2clas.setWidthPercentage(widthsTableDG2clas, rTableDG2clas);
			     tableDG2clas.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
			     cDG2clas = new PdfPCell (tableDG2clas);
			     cDG2clas.setBorder(PdfPCell.BOX);
			     cDG2clas.setHorizontalAlignment(Element.ALIGN_CENTER);
			     cDG2clas.setPadding(0); 
			 
			 

            cell = new PdfPCell(cDG2clas);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		    
		    ///////

                   
                 *Tabla con los datos del apartado 1. 
		         
		
		         PdfPTable tableDG_1 = new PdfPTable(4);
		         PdfPCell cDG_1;
                 PdfPCell cellDG_1;

                 cellDG_1 = new PdfPCell(new Paragraph("Unidad Administrativa (UA) : ",blackNegrita));
                 cellDG_1.setFixedHeight(13.33f);
                 cellDG_1.setBackgroundColor(colorLabel);
                 cellDG_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_1.addCell(cellDG_1);
		     
                 cellDG_1 = new PdfPCell(new Paragraph(""+jsonObj.get("chrUnidadAdministrativa").toString(),black));
                 cellDG_1.setFixedHeight(13.33f);
                 cellDG_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_1.addCell(cellDG_1);
		     
                 cellDG_1 = new PdfPCell(new Paragraph("Clave de la Unidad Administrativa : ",blackNegrita));
                 cellDG_1.setFixedHeight(13.33f);
                 cellDG_1.setBackgroundColor(colorLabel);
                 cellDG_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_1.addCell(cellDG_1);
		     
                 cellDG_1 = new PdfPCell(new Paragraph(""+jsonObj.get("chrCveUnidadAdm").toString(),black));
                 cellDG_1.setFixedHeight(13.33f);
                 cellDG_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_1.addCell(cellDG_1);
		     
		         float[] widthsTableDG_1 = {80f,150f,90f,150f};
			     Rectangle rTableDG_1 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDG_1.setWidthPercentage(widthsTableDG_1, rTableDG_1);
			     tableDG_1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
		         cDG_1 = new PdfPCell (tableDG_1);
		         cDG_1.setBorder(PdfPCell.BOX);
		         cDG_1.setHorizontalAlignment(Element.ALIGN_CENTER);
		         cDG_1.setPadding(0); 

            cell = new PdfPCell(cDG_1);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		    
		    
		    ////Estados e Municipio  OSC Otro
		    
		    
		    PdfPTable tableDG2OscO = new PdfPTable(6);
	        PdfPCell cDG2OscO;
            PdfPCell cellDG2OscO;
               
           cDG2OscO = new PdfPCell(new Paragraph("RFC (OSC u Otro) ",blackNegrita));
           cDG2OscO.setFixedHeight(13.33f);
           cDG2OscO.setBackgroundColor(colorLabel);
           cDG2clas.setHorizontalAlignment(Element.ALIGN_LEFT);
           tableDG2OscO.addCell(cDG2OscO);
           
               cellDG2OscO = new PdfPCell(new Paragraph(""+(jsonObj.get("chrRfc").toString() =="null"?"-":jsonObj.get("chrRfc").toString()),black));
               cellDG2OscO.setFixedHeight(13.33f);
               cellDG2OscO.setHorizontalAlignment(Element.ALIGN_LEFT);
               tableDG2OscO.addCell(cellDG2OscO);
		     
               
               cDG2OscO = new PdfPCell(new Paragraph("CLUNI (OSC) ",blackNegrita));
               cDG2OscO.setFixedHeight(13.33f);
               cDG2OscO.setBackgroundColor(colorLabel);
               cDG2clas.setHorizontalAlignment(Element.ALIGN_LEFT);
               tableDG2OscO.addCell(cDG2OscO);

                   cellDG2OscO = new PdfPCell(new Paragraph(""+(jsonObj.get("chrCluni").toString() =="null"?"-":jsonObj.get("chrCluni").toString()),black));
                   cellDG2OscO.setFixedHeight(13.33f);
                   cellDG2OscO.setHorizontalAlignment(Element.ALIGN_LEFT);
                   tableDG2OscO.addCell(cellDG2OscO);
                   
                   
                   cDG2OscO = new PdfPCell(new Paragraph("Razón Social (OSC u Otro)",blackNegrita));
                   cDG2OscO.setFixedHeight(13.33f);
                   cDG2OscO.setBackgroundColor(colorLabel);
                   cDG2clas.setHorizontalAlignment(Element.ALIGN_LEFT);
                   tableDG2OscO.addCell(cDG2OscO);

                       cellDG2OscO = new PdfPCell(new Paragraph(""+(jsonObj.get("chrRazonSocial").toString() =="null"?"-":jsonObj.get("chrRazonSocial").toString()),black));
                       cellDG2OscO.setFixedHeight(13.33f);
                       cellDG2OscO.setHorizontalAlignment(Element.ALIGN_LEFT);
                       tableDG2OscO.addCell(cellDG2OscO);
		     
		         float[] widthsTableDG2OscO = {30f,30f,30f,30f,50f,40f};
			     Rectangle rTableDG2OscO = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));

			     tableDG2OscO.setWidthPercentage(widthsTableDG2OscO, rTableDG2OscO);
			     tableDG2OscO.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
			     cDG2OscO = new PdfPCell (tableDG2OscO);
			     cDG2OscO.setBorder(PdfPCell.BOX);
			     cDG2OscO.setHorizontalAlignment(Element.ALIGN_CENTER);
			     cDG2OscO.setPadding(0); 
			 
			 

            cell = new PdfPCell(cDG2OscO);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		    
		    
		    ///////
		    
		    
		    cell = new PdfPCell(new Paragraph("Datos de contacto del titular de Unidad Administrativa  (Para OSC u Otro, registre los datos del Representante Legal u hom\u00f3logo)",blackNegrita));
		    cell.setBorder(PdfPCell.BOX);
		    cell.setBackgroundColor(backGris);
		    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		    table2.addCell(cell);	
		
		         PdfPTable tableDG_DT = new PdfPTable(4);
		         PdfPCell cDG_Dt1;
                 PdfPCell cellDG_Dt1;

                 cellDG_Dt1 = new PdfPCell(new Paragraph("Nombre : ",blackNegrita));
                 cellDG_Dt1.setFixedHeight(13.33f);
                 cellDG_Dt1.setBackgroundColor(colorLabel);
                 cellDG_Dt1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT.addCell(cellDG_Dt1);
                 
                 String nombreTitular = jsonObj.getJSONObject("datosContactoTitular").getString("tuaNombre") +
                 " " +jsonObj.getJSONObject("datosContactoTitular").getString("tuaPrimerAp") +
                 " " +jsonObj.getJSONObject("datosContactoTitular").getString("tuaSegundoAp");
		     
                 cellDG_Dt1 = new PdfPCell(new Paragraph(nombreTitular,black));
                 cellDG_Dt1.setFixedHeight(13.33f);
                 cellDG_Dt1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT.addCell(cellDG_Dt1);
		     
                 cellDG_Dt1 = new PdfPCell(new Paragraph("Cargo : ",blackNegrita));
                 cellDG_Dt1.setFixedHeight(13.33f);
                 cellDG_Dt1.setBackgroundColor(colorLabel);
                 cellDG_Dt1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT.addCell(cellDG_Dt1);
		     
                 cellDG_Dt1 = new PdfPCell(new Paragraph(jsonObj.getJSONObject("datosContactoTitular").getString("tuaCargo"),black));
                 cellDG_Dt1.setFixedHeight(13.33f);
                 cellDG_Dt1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT.addCell(cellDG_Dt1);
		     
                 tableDG_DT.setWidthPercentage(widthsTableDG_1, rTableDG_1);
                 tableDG_DT.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Dt1 = new PdfPCell (tableDG_DT);
                 cDG_Dt1.setBorder(PdfPCell.BOX);
                 cDG_Dt1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Dt1.setPadding(0); 

            cell = new PdfPCell(cDG_Dt1);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);	
		
		         PdfPTable tableDG_DT_1 = new PdfPTable(6);
		         PdfPCell cDG_Dt1_1;
                 PdfPCell cellDG_Dt1_1;

                 cellDG_Dt1_1 = new PdfPCell(new Paragraph("Tel\u00e9fono : ",blackNegrita));
                 cellDG_Dt1_1.setFixedHeight(13.33f);
                 cellDG_Dt1_1.setBackgroundColor(colorLabel);
                 cellDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT_1.addCell(cellDG_Dt1_1);
		     
                 cellDG_Dt1_1 = new PdfPCell(new Paragraph(""+jsonObj.getJSONObject("datosContactoTitular").get("tuaTel").toString(),black));
                 cellDG_Dt1_1.setFixedHeight(13.33f);
                 cellDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT_1.addCell(cellDG_Dt1_1);
                 
                 cellDG_Dt1_1 = new PdfPCell(new Paragraph("Extensi\u00f3n : ",blackNegrita));
                 cellDG_Dt1_1.setFixedHeight(13.33f);
                 cellDG_Dt1_1.setBackgroundColor(colorLabel);
                 cellDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT_1.addCell(cellDG_Dt1_1);
		     
                 cellDG_Dt1_1 = new PdfPCell(new Paragraph(""+
                		 (jsonObj.getJSONObject("datosContactoTitular").isNull("tuaExt")?"":
                		  (jsonObj.getJSONObject("datosContactoTitular").get("tuaExt").toString().equals("null")?"":
                			  jsonObj.getJSONObject("datosContactoTitular").get("tuaExt").toString())),black));
                 cellDG_Dt1_1.setFixedHeight(13.33f);
                 cellDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT_1.addCell(cellDG_Dt1_1);
                 
                 cellDG_Dt1_1 = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional : ",blackNegrita));
                 cellDG_Dt1_1.setFixedHeight(13.33f);
                 cellDG_Dt1_1.setBackgroundColor(colorLabel);
                 cellDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT_1.addCell(cellDG_Dt1_1);
		     
                 cellDG_Dt1_1 = new PdfPCell(new Paragraph(jsonObj.getJSONObject("datosContactoTitular").getString("tuaCorreo"),black));
                 cellDG_Dt1_1.setFixedHeight(13.33f);
                 cellDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT_1.addCell(cellDG_Dt1_1);
		     
                 tableDG_DT_1.setWidthPercentage(widthsTableDG, rTableDG);
                 tableDG_DT_1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Dt1_1 = new PdfPCell (tableDG_DT_1);
                 cDG_Dt1_1.setBorder(PdfPCell.BOX);
                 cDG_Dt1_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Dt1_1.setPadding(0); 

            cell = new PdfPCell(cDG_Dt1_1);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);*/	     
		    
		    
		  
		    
		    ///se elimina con el nuevo cambio..
		   /* cell = new PdfPCell(new Paragraph("Datos de contacto del enlace de UA responsable del SISI ",blackNegrita));
		    cell.setBorder(PdfPCell.BOX);
		    cell.setBackgroundColor(backGris);
		    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		    table2.addCell(cell);

		         PdfPTable tableDG_DT2 = new PdfPTable(4);
		         PdfPCell cDG_Dt2;
                 PdfPCell cellDG_Dt2;

                 cellDG_Dt2 = new PdfPCell(new Paragraph("Nombre : ",blackNegrita));
                 cellDG_Dt2.setFixedHeight(13.33f);
                 cellDG_Dt2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2.addCell(cellDG_Dt2);
		     
                 String nombreEnlace = jsonObj.getJSONObject("datosContactoEnlace").getString("chrNombreContactoEnl") +" "+
                		               jsonObj.getJSONObject("datosContactoEnlace").getString("chrPrimerAp")+" "+
                		               jsonObj.getJSONObject("datosContactoEnlace").getString("chrSegundoAp");
                 
                 cellDG_Dt2 = new PdfPCell(new Paragraph(nombreEnlace,black));
                 cellDG_Dt2.setFixedHeight(13.33f);
                 cellDG_Dt2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2.addCell(cellDG_Dt2);
		     
                 cellDG_Dt2 = new PdfPCell(new Paragraph("Cargo : ",blackNegrita));
                 cellDG_Dt2.setFixedHeight(13.33f);
                 cellDG_Dt2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2.addCell(cellDG_Dt2);
		     
                 cellDG_Dt2 = new PdfPCell(new Paragraph(jsonObj.getJSONObject("datosContactoEnlace").getString("chrCargoContactoEnl"),black));
                 cellDG_Dt2.setFixedHeight(13.33f);
                 cellDG_Dt2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2.addCell(cellDG_Dt2);
		     
                 tableDG_DT2.setWidthPercentage(widthsTableDG_1, rTableDG_1);
                 tableDG_DT2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Dt2 = new PdfPCell (tableDG_DT2);
                 cDG_Dt2.setBorder(PdfPCell.BOX);
                 cDG_Dt2.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Dt2.setPadding(0); 

            cell = new PdfPCell(cDG_Dt2);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		
		         PdfPTable tableDG_DT2_1 = new PdfPTable(6);
		         PdfPCell cDG_Dt2_1;
                 PdfPCell cellDG_Dt2_1;

                 cellDG_Dt2_1 = new PdfPCell(new Paragraph("Tel\u00e9fono : ",blackNegrita));
                 cellDG_Dt2_1.setFixedHeight(13.33f);
                 cellDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2_1.addCell(cellDG_Dt2_1);
		     
                 cellDG_Dt2_1 = new PdfPCell(new Paragraph(""+jsonObj.getJSONObject("datosContactoEnlace").get("chrTelefono").toString(),black));
                 cellDG_Dt2_1.setFixedHeight(13.33f);
                 cellDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2_1.addCell(cellDG_Dt2_1);
                 
                 cellDG_Dt2_1 = new PdfPCell(new Paragraph("Extensi\u00f3n : ",blackNegrita));
                 cellDG_Dt2_1.setFixedHeight(13.33f);
                 cellDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2_1.addCell(cellDG_Dt2_1);
		     
                 cellDG_Dt2_1 = new PdfPCell(new Paragraph(
                		 (!jsonObj.getJSONObject("datosContactoEnlace").has("chrExtensionContactoEnl")?"":
                		 (jsonObj.getJSONObject("datosContactoEnlace").isNull("chrExtensionContactoEnl")?"":
                   		  (jsonObj.getJSONObject("datosContactoEnlace").get("chrExtensionContactoEnl").toString().equals("null")?"":
                   			  jsonObj.getJSONObject("datosContactoEnlace").get("chrExtensionContactoEnl").toString()))),black));

                 cellDG_Dt2_1.setFixedHeight(13.33f);
                 cellDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2_1.addCell(cellDG_Dt2_1);
                 
                 cellDG_Dt2_1 = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional : ",blackNegrita));
                 cellDG_Dt2_1.setFixedHeight(13.33f);
                 cellDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2_1.addCell(cellDG_Dt2_1);
		     
                 cellDG_Dt2_1 = new PdfPCell(new Paragraph(jsonObj.getJSONObject("datosContactoEnlace").getString("chrEmail"),black));
                 cellDG_Dt2_1.setFixedHeight(13.33f);
                 cellDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_DT2_1.addCell(cellDG_Dt2_1);
		     
                 tableDG_DT2_1.setWidthPercentage(widthsTableDG, rTableDG);
                 tableDG_DT2_1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Dt2_1 = new PdfPCell (tableDG_DT2_1);
                 cDG_Dt2_1.setBorder(PdfPCell.BOX);
                 cDG_Dt2_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Dt2_1.setPadding(0); 

            cell = new PdfPCell(cDG_Dt2_1);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);*/
		    
		    /*
		    cell = new PdfPCell(new Paragraph("Datos de contacto del Enlace T\u00e9cnico ",blackNegrita));
		    cell.setBorder(PdfPCell.BOX);
		    cell.setBackgroundColor(backGris);
		    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		    table2.addCell(cell);

		    JSONArray arrayContEnlTec = jsonObj.getJSONArray("datosContactosEnlTecArray");
		    
		    for(int i=0;i<arrayContEnlTec.length();i++){
		    
		         PdfPTable tableDG_Et = new PdfPTable(4);
		         PdfPCell cDG_Et;
                 PdfPCell cellDG_Et;

                 cellDG_Et = new PdfPCell(new Paragraph("Nombre : ",blackNegrita));
                 cellDG_Et.setFixedHeight(13.33f);
                 cellDG_Et.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et.addCell(cellDG_Et);
                 
                 String nombreEnlaceTec = arrayContEnlTec.getJSONObject(i).getString("chrNombreConEnlTecnico") +" "+
                		 arrayContEnlTec.getJSONObject(i).getString("chrPrimerAp")+" "+
                		 arrayContEnlTec.getJSONObject(i).getString("chrSegundoAp");
		     
                 cellDG_Et = new PdfPCell(new Paragraph(nombreEnlaceTec,black));
                 cellDG_Et.setFixedHeight(13.33f);
                 cellDG_Et.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et.addCell(cellDG_Et);
		     
                 cellDG_Et = new PdfPCell(new Paragraph("Cargo : ",blackNegrita));
                 cellDG_Et.setFixedHeight(13.33f);
                 cellDG_Et.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et.addCell(cellDG_Et);
		     
                 cellDG_Et = new PdfPCell(new Paragraph(arrayContEnlTec.getJSONObject(i).getString("chrCargoConEnlTecnico"),black));
                 cellDG_Et.setFixedHeight(13.33f);
                 cellDG_Et.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et.addCell(cellDG_Et);
		     
                 tableDG_Et.setWidthPercentage(widthsTableDG_1, rTableDG_1);
                 tableDG_Et.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Et = new PdfPCell (tableDG_Et);
                 cDG_Et.setBorder(PdfPCell.BOX);
                 cDG_Et.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Et.setPadding(0); 

            cell = new PdfPCell(cDG_Et);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		
		         PdfPTable tableDG_Et_1 = new PdfPTable(6);
		         PdfPCell cDG_Et_1;
                 PdfPCell cellDG_Et_1;

                 cellDG_Et_1 = new PdfPCell(new Paragraph("Tel\u00e9fono : ",blackNegrita));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 cellDG_Et_1 = new PdfPCell(new Paragraph(""+arrayContEnlTec.getJSONObject(i).get("chrTelefonoConEnlTec").toString(),black));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
                 
                 cellDG_Et_1 = new PdfPCell(new Paragraph("Extensi\u00f3n : ",blackNegrita));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 cellDG_Et_1 = new PdfPCell(new Paragraph(""+arrayContEnlTec.getJSONObject(i).get("chrExtensionConEnlTecnico").toString(),black));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
                 
                 cellDG_Et_1 = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional : ",blackNegrita));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 cellDG_Et_1 = new PdfPCell(new Paragraph(arrayContEnlTec.getJSONObject(i).getString("chrEmailConEnlTecnico"),black));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 tableDG_Et_1.setWidthPercentage(widthsTableDG, rTableDG);
                 tableDG_Et_1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Et_1 = new PdfPCell (tableDG_Et_1);
                 cDG_Et_1.setBorder(PdfPCell.BOX);
                 cDG_Et_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Et_1.setPadding(0); 
                 
                 
                 
                 cellDG_Et_1 = new PdfPCell(new Paragraph("Integraci\u00f3n de Padrones : ",blackNegrita));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 cellDG_Et_1 = new PdfPCell(new Paragraph("SI",black));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 tableDG_Et_1.addCell(cellDG_Et_1);
                 
                 cellDG_Et_1 = new PdfPCell(new Paragraph("Informaci\u00f3n Geoespacial : ",blackNegrita));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 cellDG_Et_1 = new PdfPCell(new Paragraph("SI",black));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 tableDG_Et_1.addCell(cellDG_Et_1);
                 
                 cellDG_Et_1 = new PdfPCell(new Paragraph("Info.Socioecon\u00f3mica/\nFocalizacion : ",blackNegrita));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 cellDG_Et_1 = new PdfPCell(new Paragraph("NO",black));
                 cellDG_Et_1.setFixedHeight(13.33f);
                 cellDG_Et_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 tableDG_Et_1.addCell(cellDG_Et_1);
		     
                 tableDG_Et_1.setWidthPercentage(widthsTableDG, rTableDG);
                 tableDG_Et_1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cDG_Et_1 = new PdfPCell (tableDG_Et_1);
                 cDG_Et_1.setBorder(PdfPCell.BOX);
                 cDG_Et_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cDG_Et_1.setPadding(0);

            cell = new PdfPCell(cDG_Et_1);
		    cell.setBorder(PdfPCell.BOX);
		    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		    table2.addCell(cell);
		    
		    }*/
		    
		    /*Fin II. Datos generales*/
		    
		    /*Recuperacion de Datos Insumos*/
/*		    boolean existeInsumos = jsonObj.has("insumosRegistroAdhesions")?(jsonObj.getJSONArray("insumosRegistroAdhesions").length()>0?true:false):false; 
	        JSONArray arrayProgramasSoc = new JSONArray();
	        JSONArray arrayProgramasSoc_PSInf = new JSONArray();
	        JSONObject arrayEnlTecSoc = new JSONObject();
	        
	        
	        JSONArray arrayUnidades = new JSONArray();
	        JSONArray arrayUnidades_Inf = new JSONArray();
	        
	        JSONArray arrayAcciones = new JSONArray();
	        JSONArray arrayAcciones_Inf = new JSONArray();
	        
	        if(existeInsumos){
	        	JSONArray arrayProgramasSocTmp = jsonObj.getJSONArray("insumosRegistroAdhesions");
	        	System.out.print("MOdeloArray: " +arrayProgramasSocTmp.toString());
	        	int cntEnl=0;
	        	for(int i=0;i<arrayProgramasSocTmp.length();i++){
	        		//Se recuperan los programas sociales
	        		if(arrayProgramasSocTmp.getJSONObject(i).getInt("snIntercambiaInfSisi")==1){
	        			JSONObject jsonPrograma = new JSONObject();//
	        			jsonPrograma.put("datosResponsable",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("programasSocResponsablePros").getJSONObject(0).getJSONObject("responsableProgramaSocial"));
	        			jsonPrograma.put("datosPrograma",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("programaCuaps").getJSONObject(0));
	        			jsonPrograma.put("datosEnlaces",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("datosContactoEnlTecnicos"));
	        			arrayProgramasSoc.put(jsonPrograma); 
	        		    //mi desmadre jejejeja
	        			//System.out.println("\n JsonENlacesObject0::"+arrayProgramasSocTmp.getJSONObject(i).getJSONArray("datosContactoEnlTecnicos").getJSONObject(0));
	        			 cntEnl=cntEnl+1;
	        		    arrayEnlTecSoc.put("datosEnlaces"+cntEnl,arrayProgramasSocTmp.getJSONObject(i).getJSONArray("datosContactoEnlTecnicos"));
	        		   // System.out.println("arrayEnlTecSoc"+arrayEnlTecSoc.getJSONArray("datosEnlaces"+cntEnl).toString());
	        		    
	        		   JSONArray values = jsonPrograma.getJSONArray("datosEnlaces");
	        		   for (int f = 0; f < values.length(); f++) {
	        		   
	        		   JSONObject animal = values.getJSONObject(f);
	        		   
	        		    String species = animal.getString("chrCargoConEnlTecnico");
	        		    String name = animal.getString("chrSegundoAp");

	        		    System.out.println( ", " + species + ", " + name);
	        		   
	        		   }
	        		 
	        		
	        			JSONObject jsonInfo = new JSONObject();
	        			jsonInfo.put("programaInfAportaraSisis", arrayProgramasSocTmp.getJSONObject(i).getJSONArray("programaInfAportaraSisis"));
	        			arrayProgramasSoc_PSInf.put(jsonInfo);
	        		}
	        		//Se recuperan las unidades
	        		if(arrayProgramasSocTmp.getJSONObject(i).getInt("snIntercambiaInfSisi")==2){
	        			JSONObject jsonUnidad = new JSONObject();
	        			jsonUnidad.put("datosResponsable",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("programasSocResponsablePros").getJSONObject(0).getJSONObject("responsableProgramaSocial"));
	        			jsonUnidad.put("datosPrograma",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("uaSinProgramas").getJSONObject(0));
	        			//jsonUnidad.put("datosEnlaces",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("datosContactoEnlTecnicos").getJSONObject(0));
	        			arrayUnidades.put(jsonUnidad); 
	        			
	        			JSONObject jsonInfo = new JSONObject();
	        			jsonInfo.put("programaInfAportaraSisis", arrayProgramasSocTmp.getJSONObject(i).getJSONArray("programaInfAportaraSisis"));
	        			arrayUnidades_Inf.put(jsonInfo);
	        		}
	        		//aCCIOES Fondos estrategias
	        		if(arrayProgramasSocTmp.getJSONObject(i).getInt("snIntercambiaInfSisi")==3){
	        			JSONObject jsonAccion = new JSONObject();
	        			jsonAccion.put("datosResponsable",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("accionesFonResponsable").getJSONObject(0).getJSONObject("responsableAccionesFondos"));
	        			jsonAccion.put("datosPrograma",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("accionesFondos").getJSONObject(0));
	        			//jsonAccion.put("datosEnlaces",arrayProgramasSocTmp.getJSONObject(i).getJSONArray("datosContactoEnlTecnicos").getJSONObject(0));
	        			arrayAcciones.put(jsonAccion); 
	        			
	        			JSONObject jsonInfo = new JSONObject();
	        			jsonInfo.put("programaInfAportaraSisis", arrayProgramasSocTmp.getJSONObject(i).getJSONArray("programaInfAportaraSisis"));
	        			arrayAcciones_Inf.put(jsonInfo);
	        		}
	        	}
	        }
		    
		    
		    
		    Inicio III. Programas Sociales
		    
		    cell = new PdfPCell(new Paragraph("III. Programas Sociales",nomCampo));
			cell.setBorder(PdfPCell.YMARK);
			cell.setBackgroundColor(myColor);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(cell);

		         PdfPTable tablePS_DT = new PdfPTable(1);
		         PdfPCell cPS_Dt;
                 PdfPCell cellPS_Dt;

                 String leyenda1 = "Registre el total de programas sociales que opera o de los cuales concentra informaci\u00f3n.";
                 
                 cellPS_Dt = new PdfPCell(new Paragraph(leyenda1,blackNegrita));
                 cellPS_Dt.setFixedHeight(20f);
                 cellPS_Dt.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_DT.addCell(cellPS_Dt);
	
                 float[] widthsTablePS = {470f};
				 Rectangle rTablePS = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
                 tablePS_DT.setWidthPercentage(widthsTablePS, rTablePS);
                 tablePS_DT.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
                 cPS_Dt = new PdfPCell (tablePS_DT);
                 cPS_Dt.setBorder(PdfPCell.BOX);
                 cPS_Dt.setHorizontalAlignment(Element.ALIGN_CENTER);
                 cPS_Dt.setPadding(0); 
            
                 
            cell = new PdfPCell(cPS_Dt);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);

		         PdfPTable tablePS_Grid = new PdfPTable(11);
		         PdfPCell cPS_Grid;
                 PdfPCell cellPS_Grid;

                 cellPS_Grid = new PdfPCell(new Paragraph("N\u00famero de Programa",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Clave del Programa",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Nombre del Programa",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Dependencia",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Nombre del Titular del Programa",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Cargo",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Tel\u00e9fono",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Extensi\u00f3n",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Estatus",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 cellPS_Grid = new PdfPCell(new Paragraph("Motivo de baja",blackNegrita));
                 cellPS_Grid.setFixedHeight(30f);
                 cellPS_Grid.setBackgroundColor(backGris);
                 cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tablePS_Grid.addCell(cellPS_Grid);
                 
                 for(int i=0;i<arrayProgramasSoc.length();i++){
                	 
                	 int cnt = i+1;
                	 cellPS_Grid = new PdfPCell(new Paragraph(""+ cnt ,black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_CENTER);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosPrograma").getString("chrClavePrespupuestalPro"),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosPrograma").getString("chrNombreProgramaCuaps"),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosPrograma").getString("chrDescripcionDependencia"),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     String aNombreTitular = arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").getString("rpNombre")+" " +
                    		 arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").getString("rpPrimerAp")+" " +
                             arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").getString("rpSegundoAp");
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(aNombreTitular,black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCargo"),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").get("rpTel").toString(),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(
                    		 (!arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").has("rpExt")?"":
                        		 (arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").isNull("rpExt")?"":
                           		  (arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString().equals("null")?"":
                           			arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString()))),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCorreo"),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosPrograma").get("baja").toString().equals("1")?"Baja":"Activo",black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
                     
                     cellPS_Grid = new PdfPCell(new Paragraph(arrayProgramasSoc.getJSONObject(i).getJSONObject("datosPrograma").get("chrMotivoBaja").toString().equals("null")?" ":arrayProgramasSoc.getJSONObject(i).getJSONObject("datosPrograma").get("chrMotivoBaja").toString(),black));
                     cellPS_Grid.setFixedHeight(40f);
                     cellPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tablePS_Grid.addCell(cellPS_Grid);
     		     }
                 
                 
	
                 float[] widthsTablePS_Grid = {52f,52f,52f,52f,52f,52f,50f,33f,52f,25f,45f};
				 Rectangle rTablePS_Grid = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tablePS_Grid.setWidthPercentage(widthsTablePS_Grid, rTablePS_Grid);
				 tablePS_Grid.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cPS_Grid = new PdfPCell (tablePS_Grid);
				 cPS_Grid.setBorder(PdfPCell.BOX);
				 cPS_Grid.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cPS_Grid.setPadding(0); 
            
                 
            cell = new PdfPCell(cPS_Grid);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);
     		
     		//enlaces tecnicos programas sociales
     		
     		String leyendEnlTec = "Por cada programa social que registr\u00f3, designe a tres Enlaces, uno por tipo de informaci\u00f3n: \n "+
     				"Responsable de Integraci\u00f3n de Padrones, Informaci\u00f3n geoespacial e Informaci\u00f3n Socioecon\u00f3mica y Focalizaci\u00f3n.";
     		
     		cell = new PdfPCell(new Paragraph(leyendEnlTec,blackNegrita));
     		cell.setBorder(PdfPCell.YMARK);
     		cell.setFixedHeight(30f);
     		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
     		table2.addCell(cell);
     		
     		
     		  PdfPTable tableAF_Grid = new PdfPTable(4);
		      PdfPCell cAF_Grid;
              PdfPCell cellAF_Grid;

              cellAF_Grid = new PdfPCell(new Paragraph("N\u00famero de Programa",blackNegrita));
              cellAF_Grid.setFixedHeight(40f);
              cellAF_Grid.setBackgroundColor(backGris);
              cellAF_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
              tableAF_Grid.addCell(cellAF_Grid);
              
              cellAF_Grid = new PdfPCell(new Paragraph("Responsable de Integraci\u00f3n de Padrones",blackNegrita));
              cellAF_Grid.setFixedHeight(40f);
              cellAF_Grid.setBackgroundColor(backGris);
              cellAF_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
              tableAF_Grid.addCell(cellAF_Grid);
     		
              cellAF_Grid = new PdfPCell(new Paragraph("Responsable de Informaci\u00f3n Geoespacial",blackNegrita));
              cellAF_Grid.setFixedHeight(40f);
              cellAF_Grid.setBackgroundColor(backGris);
              cellAF_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
              tableAF_Grid.addCell(cellAF_Grid);
     		
              cellAF_Grid = new PdfPCell(new Paragraph("Responsable de Informaci\u00f3n Socioecon\u00f3mica y Focalizaci\u00f3n",blackNegrita));
              cellAF_Grid.setFixedHeight(40f);
              cellAF_Grid.setBackgroundColor(backGris);
              cellAF_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
              tableAF_Grid.addCell(cellAF_Grid);
              
       
            JSONArray arrayIndsumo = jsonObj.getJSONArray("insumosRegistroAdhesions");
        	for(int i=0;i<arrayIndsumo.length();i++){
          	
        		if(arrayIndsumo.getJSONObject(i).getInt("snIntercambiaInfSisi")==1){
    			JSONObject jsonEnlaces = new JSONObject();//
    			
    			jsonEnlaces.put("datosEnlaces",arrayIndsumo.getJSONObject(i).getJSONArray("datosContactoEnlTecnicos"));
    			
    			   JSONArray enl = jsonEnlaces.getJSONArray("datosEnlaces");
    			   
    			   
    			   System.out.println("ArrayEnlaces: "+enl.toString());
    			   
    			   for (int f = 0; f < enl.length(); f++) {

    	            	  JSONObject enlace = enl.getJSONObject(f);
    	            	  
    	         		    String aNombreEnlace = enlace.getString("chrNombreConEnlTecnico")+" "+enlace.getString("chrPrimerAp")+" "+enlace.getString("chrSegundoAp");
    	         		    String cargoEnlace = enlace.getString("chrCargoConEnlTecnico");
    	         		    String telefonoEnlace = enlace.get("chrTelefonoConEnlTec").toString();
    	         		    String extEnlace = enlace.isNull("chrExtensionConEnlTecnico")?"":enlace.get("chrExtensionConEnlTecnico").toString();
    	         		    String emailEnlace = enlace.getString("chrEmailConEnlTecnico");
    	         		   // String num = enlace.get("num").toString();
    	         	
    	             	 int cnt = f+1;
    	             	 int cn = i+1;
    	             	 int cntEnl =0; 
    	             	if(cnt==1) {
    	             
      	             	  cellAF_Grid = new PdfPCell(new Paragraph("" ,black));
      	                  cellAF_Grid.setFixedHeight(50f);
      	                  cellAF_Grid.setHorizontalAlignment(Element.ALIGN_CENTER);
      	                  //cellAF_Grid.setRowspan(5);
      	                  tableAF_Grid.addCell(cellAF_Grid);
      	                  
      	             	}

    	                 System.out.println("sada"+cnt + aNombreEnlace);

    	                  PdfPTable table = new PdfPTable(4);
    	                  table.setWidths(new int[]{ 1, 1, 1, 1});
    	                  PdfPCell c1 = new PdfPCell(new Phrase("Nombre",black));
    	                  c1.setBackgroundColor(colorLabel);
    	                  c1.setColspan(1);
    	                  table.addCell(c1);
    	                  
    	                  
    	                  String aNombreEnlace = arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").getString("chrNombreConEnlTecnico")+" " +
    	                		  arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").getString("chrPrimerAp")+" " +
    	                		  arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").getString("chrSegundoAp");
    	                  
    	                  PdfPCell c1N = new PdfPCell(new Phrase(aNombreEnlace,black));
    	                  c1N.setColspan(3);
    	                  table.addCell(c1N);
    	                  
    	                 
    	                  PdfPCell cC = new PdfPCell(new Phrase("Cargo",black));
    	                  cC.setColspan(1);
    	                  cC.setBackgroundColor(colorLabel);
    	                  table.addCell(cC);
    	                  
    	                  PdfPCell cC2 = new PdfPCell(new Phrase(cargoEnlace,black));
    	                  cC2.setColspan(3);
    	                  table.addCell(cC2);
    	                  // Then follows a row with normal cells
    	                  PdfPCell cC3 = new PdfPCell(new Phrase("Tel\u00e9fono",black));
    	                  cC3.setBackgroundColor(colorLabel);
    	                  table.addCell(cC3);
    	                 // table.addCell(new Paragraph("Tel\u00e9fono" ,black));
    	                  table.addCell(new Paragraph(telefonoEnlace ,black));
    	                  //table.addCell(new Paragraph("Extensi\u00f3n" ,black));
    	                  PdfPCell cC4 = new PdfPCell(new Phrase("Extensi\u00f3n",black));
    	                  cC4.setBackgroundColor(colorLabel);
    	                  table.addCell(cC4);
    	                  
    	                  String ext = (!arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").has("chrExtensionConEnlTecnico")?"":
    	             		 (arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").isNull("chrExtensionConEnlTecnico")?"":
    	                  		  (arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").get("chrExtensionConEnlTecnico").toString().equals("null")?"":
    	                  			arrayProgramasSoc.getJSONObject(i).getJSONObject("datosEnlaces").get("chrExtensionConEnlTecnico").toString())));
    	                  
    	                  table.addCell(new Paragraph(extEnlace,black));
    	                  // Again we have a row with 1 cell that spans 3 columns
    	                  PdfPCell c5 = new PdfPCell(new Phrase("Correo electr\u00f3nico institucional",black));
    	                  c5.setColspan(2);
    	                  c5.setBackgroundColor(colorLabel);
    	                  table.addCell(c5);
    	                  
    	                  PdfPCell cC5 = new PdfPCell(new Phrase(emailEnlace,black));
    	                  cC5.setColspan(2);
    	                  cC5.setBackgroundColor(colorLabel);
    	                  table.addCell(cC5);
    	                 
    	                  cellAF_Grid = new PdfPCell(table);
    	                  cellAF_Grid.setFixedHeight(50f);
    	                  tableAF_Grid.addCell(cellAF_Grid);
    			   
    	             	
          	
          	    }
        	}
                   
                  
            }
              
              
              
              float[] widthsTableAF_Grid = {25f,100f,100f,100f};
				 Rectangle rTableAF_Grid = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tableAF_Grid.setWidthPercentage(widthsTableAF_Grid, rTableAF_Grid);
				 tableAF_Grid.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cAF_Grid = new PdfPCell (tableAF_Grid);
				 cAF_Grid.setBorder(PdfPCell.BOX);
				 cAF_Grid.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cAF_Grid.setPadding(0); 
     		
			    cell = new PdfPCell(cAF_Grid);
		 		cell.setBorder(PdfPCell.BOX);
		    	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		   		table2.addCell(cell);
		   		
		   		
		   		
     		
     		/////
     		String leyenda2 = "Por cada programa social que registr\u00f3, seleccione la informaci\u00f3n que aportar\u00e1 al SISI.";
            
     		cell = new PdfPCell(new Paragraph(leyenda2,blackNegrita));
     		cell.setBorder(PdfPCell.YMARK);
     		cell.setFixedHeight(20f);
     		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
     		table2.addCell(cell);
     		
		         PdfPTable tablePS_Grid2 = new PdfPTable(5);
		         PdfPCell cPS_Grid2;
                 PdfPCell cellPS_Grid2;

                 cellPS_Grid2 = new PdfPCell(new Paragraph("N\u00famero de programa",blackNegrita));
                 cellPS_Grid2.setFixedHeight(40f);
                 cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellPS_Grid2.setBackgroundColor(backGris);
                 tablePS_Grid2.addCell(cellPS_Grid2);
                 
                 cellPS_Grid2 = new PdfPCell(new Paragraph("Recolecci\u00f3n de CUIS",blackNegrita));
                 cellPS_Grid2.setFixedHeight(40f);
                 cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellPS_Grid2.setBackgroundColor(backGris);
                 tablePS_Grid2.addCell(cellPS_Grid2);
                 
                 cellPS_Grid2 = new PdfPCell(new Paragraph("Informaci\u00f3n socioecon\u00f3mica",blackNegrita));
                 cellPS_Grid2.setFixedHeight(40f);
                 cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellPS_Grid2.setBackgroundColor(backGris);
                 tablePS_Grid2.addCell(cellPS_Grid2);
                 
                 cellPS_Grid2 = new PdfPCell(new Paragraph("Padr\u00f3n de Beneficiarios",blackNegrita));
                 cellPS_Grid2.setFixedHeight(40f);
                 cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellPS_Grid2.setBackgroundColor(backGris);
                 tablePS_Grid2.addCell(cellPS_Grid2);
                 
                 cellPS_Grid2 = new PdfPCell(new Paragraph("Infraestructura Social /Domicilios geográficos /Obras FAIS /Información Georreferenciada para el Desarrollo Social.",blackNegrita));
                 cellPS_Grid2.setFixedHeight(40f);
                 cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellPS_Grid2.setBackgroundColor(backGris);
                 tablePS_Grid2.addCell(cellPS_Grid2);
                 
                 for(int i=0;i<arrayProgramasSoc_PSInf.length();i++){
         		 
                    int cnt = i+1;

     		    	String recoleccion = "NO";
    		    	String infoSE = "NO";
    		    	String padron = "NO";
    		    	String viviendas = "NO";
    		    	 
    		    	for(int j=0;j<arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		recoleccion = arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==1?"SI":"NO";
    		    		if(recoleccion.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		infoSE = arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==2?"SI":"NO";
    		    		if(infoSE.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		padron = arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==4?"SI":"NO";
    		    		if(padron.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		viviendas = arrayProgramasSoc_PSInf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==5?"SI":"NO";
    		    		if(viviendas.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	
    		    	cellPS_Grid2 = new PdfPCell(new Paragraph(""+ cnt,black));
                    cellPS_Grid2.setFixedHeight(20f);
                    cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablePS_Grid2.addCell(cellPS_Grid2);
                    
                    
                     ------------------------------------- 
                    cellPS_Grid2 = new PdfPCell(new Paragraph(recoleccion,black));
                    cellPS_Grid2.setFixedHeight(20f);
                    cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablePS_Grid2.addCell(cellPS_Grid2);
                    
                    cellPS_Grid2 = new PdfPCell(new Paragraph(infoSE,black));
                    cellPS_Grid2.setFixedHeight(20f);
                    cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablePS_Grid2.addCell(cellPS_Grid2);
                    
                    cellPS_Grid2 = new PdfPCell(new Paragraph(padron,black));
                    cellPS_Grid2.setFixedHeight(20f);
                    cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablePS_Grid2.addCell(cellPS_Grid2);
                    
                    cellPS_Grid2 = new PdfPCell(new Paragraph(viviendas,black));
                    cellPS_Grid2.setFixedHeight(20f);
                    cellPS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tablePS_Grid2.addCell(cellPS_Grid2);
    		    	
    		    	
                 }
                 
    
                 float[] widthsTablePS_Grid2 = {30f,110f,110f,110f,110f};
				 Rectangle rTablePS_Grid2 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tablePS_Grid2.setWidthPercentage(widthsTablePS_Grid2, rTablePS_Grid2);
				 tablePS_Grid2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cPS_Grid2 = new PdfPCell (tablePS_Grid2);
				 cPS_Grid2.setBorder(PdfPCell.BOX);
				 cPS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cPS_Grid2.setPadding(0); 
                 
            cell = new PdfPCell(cPS_Grid2);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);
     		     
     		
     		Fin III. Programas Sociales
     		
     	    Inicio IV. Acciones, Fondos y Estrategias
     			
     		cell = new PdfPCell(new Paragraph("IV. Acciones, Fondos, Estrategias o Proyectos (AFEP).",nomCampo));
			cell.setBorder(PdfPCell.BOX);
			cell.setBackgroundColor(myColor);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(cell);
			
			String leyendaAFE = "Registre las Acciones, Fondos, Estrategias o Proyectos que opere y que intercambiar\u00e1n informaci\u00f3n con el SISI.";
     		
			cell = new PdfPCell(new Paragraph(leyendaAFE,blackNegrita));
            cell.setBorder(PdfPCell.BOX);
            cell.setFixedHeight(20f);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table2.addCell(cell);
            
            PdfPTable tableAFE_Grid1 = new PdfPTable(4);
	        PdfPCell  cAFE_Grid1;
            PdfPCell  cellAFE_Grid1;
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph("a. Acci\u00f3n",blackNegrita));
            cellAFE_Grid1.setFixedHeight(13.33f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellAFE_Grid1.setBackgroundColor(backGris);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph("b. Fondo",blackNegrita));
            cellAFE_Grid1.setFixedHeight(13.33f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellAFE_Grid1.setBackgroundColor(backGris);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph("c. Estrategia",blackNegrita));
            cellAFE_Grid1.setFixedHeight(13.33f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellAFE_Grid1.setBackgroundColor(backGris);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph("d. Proyecto",blackNegrita));
            cellAFE_Grid1.setFixedHeight(13.33f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            cellAFE_Grid1.setBackgroundColor(backGris);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            //////
            String a ="Se refiere a las Acciones de Desarrollo Social de los tres \u00f3rdenes de gobierno (con excepci\u00f3n de los programas presupuestarios federales clasificaci\u00f3n E y B)  que est\u00e9n alineadas a alguno de los derechos sociales o la dimensi\u00f3n de bienestar econ\u00f3mico, y que otorguen transferencias monetarias, bienes o servicios a personas f\u00edsicas o morales.\r\n" + 
            		"\r\n Ejemplo: Entrega de Despensas, Promoci\u00f3n de la Salud";
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph(a,black));
            cellAFE_Grid1.setFixedHeight(80f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
            String b ="Recursos públicos correspondientes al gasto federalizado (Fondos de Aportaciones Federales), transferidos a las Entidades Federativas, los Municipios y las demarcaciones territoriales de la Ciudad de M\u00e9xico, quienes los ejercen, administran y destinan a los fines establecidos en la Ley de Coordinaci\u00f3n Fiscal, según el fondo correspondiente. As\u00ed como aquellos Fondos de origen Estatal que transfieran recursos a los municipios.\r\n" + 
            		"Ejemplo: Fondo de Aportaciones para la Infraestructura Social (FAIS), FISE/FISM";
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph(b,black));
            cellAFE_Grid1.setFixedHeight(80f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
            String c ="Mecanismo de coordinaci\u00f3n entre Dependencias gubernamentales para orientar las intervenciones de los Programas o Acciones de Desarrollo Social hacia una poblaci\u00f3n, problem\u00e1tica o regi\u00f3n espec\u00edficas.\r\n" + 
            		"\r\n Ejemplo: Estrategia Nacional de Inclusi\u00f3n";
            
            cellAFE_Grid1 = new PdfPCell(new Paragraph(c,black));
            cellAFE_Grid1.setFixedHeight(80f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
            String d="Plan o intervenci\u00f3n enfocada a solucionar una problem\u00e1tica social concreta, que re\u00fane un conjunto de actividades coordinadas y encaminadas a alcanzar objetivos espec\u00edficos en un lugar y tiempo delimitado, generalmente a corto plazo. Se compone de las siguientes etapas: diagn\u00f3stico, dise\u00f1o, implementaci\u00f3n y evaluaci\u00f3n de los resultados.\r\n" + 
            		"Ejemplo: Proyecto de asilo, alimentaci\u00f3n y recreaci\u00f3n para 200 Adultos Mayores en el municipio de Chalco, Estado de M\u00e9 xico.";
           
            cellAFE_Grid1 = new PdfPCell(new Paragraph(d,black));
            cellAFE_Grid1.setFixedHeight(80f);
            cellAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
            tableAFE_Grid1.addCell(cellAFE_Grid1);
            
        
            float[] widthsTableAFE_Grid1 = {110f,130f,90f,70f};
			 Rectangle rTableAFE_Grid1 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
	     
			 tableAFE_Grid1.setWidthPercentage(widthsTableAFE_Grid1, rTableAFE_Grid1);
			 tableAFE_Grid1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
	     
			 cAFE_Grid1 = new PdfPCell (tableAFE_Grid1);
			 cAFE_Grid1.setBorder(PdfPCell.BOX);
			 cAFE_Grid1.setHorizontalAlignment(Element.ALIGN_CENTER);
			 cAFE_Grid1.setPadding(0); 
            
        cell = new PdfPCell(cAFE_Grid1);
		cell.setBorder(PdfPCell.BOX);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(cell);
		
		////////////Acciones Fondos Estrategias
		   
        PdfPTable tableAFE_Grid2 = new PdfPTable(8);
        PdfPCell cAFE_Grid2;
        PdfPCell cellAFE_Grid2;
        
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Numero de AFEP",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("ID AFEP",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Tipo",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Nombre",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Descripción",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
       
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Nombre del Área Responsable",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Estatus",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
        
        cellAFE_Grid2 = new PdfPCell(new Paragraph("Motivo de baja",blackNegrita));
        cellAFE_Grid2.setFixedHeight(30f);
        cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid2.setBackgroundColor(backGris);
        tableAFE_Grid2.addCell(cellAFE_Grid2);
    
       
       for(int i=0;i<arrayAcciones.length();i++){
    	   
    	   int cnt = i+1;
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(""+cnt,black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getString("cscIdAfe"),black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   int cscTipo = arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getInt("cscTipo");
    	   String tipo="";
    	   if(cscTipo==0){
    		   tipo="Accion";
    	   }else if(cscTipo==1){
    		   tipo="Fondo";
    	   }else if(cscTipo==2){
    		   tipo="Estrategia";
    	   }else {
    		   tipo="Proyecto";
    	   }
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(tipo,black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getString("chrNombre"),black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getString("chrDescripcion"),black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getString("chrNomAreaResAfe"),black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getString("baja"),black));
    	   cellAFE_Grid2.setFixedHeight(30f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").getString("chrMotivoBaja"),black));
    	   cellAFE_Grid2.setFixedHeight(30f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").get("baja").toString().equals("1")?"Baja":"Activo",black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
    	   cellAFE_Grid2 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").get("chrMotivoBaja").toString().equals("null")?" ":arrayAcciones.getJSONObject(i).getJSONObject("datosPrograma").get("chrMotivoBaja").toString(),black));
    	   cellAFE_Grid2.setFixedHeight(40f);
    	   cellAFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
    	   tableAFE_Grid2.addCell(cellAFE_Grid2);
    	   
       }
		
        
        float[] widthsTableAFE_Grid2 = {30f,30f,80f,100f,100f,100f,25f,30f};
		 Rectangle rTableAFE_Grid2 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
    
		 tableAFE_Grid2.setWidthPercentage(widthsTableAFE_Grid2, rTableAFE_Grid2);
		 tableAFE_Grid2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
    
		 cAFE_Grid2 = new PdfPCell (tableAFE_Grid2);
		 cAFE_Grid2.setBorder(PdfPCell.BOX);
		 cAFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
		 cAFE_Grid2.setPadding(0); 
       
        cell = new PdfPCell(cAFE_Grid2);
	    cell.setBorder(PdfPCell.BOX);
	    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table2.addCell(cell);

		
		String leyendaAFEnl = "Por cada Acci\u00f3n, Fondo, Estrategia o Proyecto que registr\u00f3, designe a un Enlace:";
 		
		cell = new PdfPCell(new Paragraph(leyendaAFEnl,blackNegrita));
        cell.setBorder(PdfPCell.BOX);
        cell.setFixedHeight(20f);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table2.addCell(cell);
	
		   
        PdfPTable tableAFE_Grid3 = new PdfPTable(6);
        PdfPCell  cAFE_Grid3;
        PdfPCell  cellAFE_Grid3;
        
        cellAFE_Grid3 = new PdfPCell(new Paragraph("N\u00famero de AFEP",blackNegrita));
        cellAFE_Grid3.setFixedHeight(30f);
        cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid3.setBackgroundColor(backGris);
        tableAFE_Grid3.addCell(cellAFE_Grid3);
        
        cellAFE_Grid3 = new PdfPCell(new Paragraph("Nombre de Enlace AFEP",blackNegrita));
        cellAFE_Grid3.setFixedHeight(30f);
        cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid3.setBackgroundColor(backGris);
        tableAFE_Grid3.addCell(cellAFE_Grid3);
        
        cellAFE_Grid3 = new PdfPCell(new Paragraph("Cargo",blackNegrita));
        cellAFE_Grid3.setFixedHeight(30f);
        cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid3.setBackgroundColor(backGris);
        tableAFE_Grid3.addCell(cellAFE_Grid3);
        
        cellAFE_Grid3 = new PdfPCell(new Paragraph("Tel\u00e9fono",blackNegrita));
        cellAFE_Grid3.setFixedHeight(30f);
        cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid3.setBackgroundColor(backGris);
        tableAFE_Grid3.addCell(cellAFE_Grid3);
      
        cellAFE_Grid3 = new PdfPCell(new Paragraph("Extensi\u00f3n",blackNegrita));
        cellAFE_Grid3.setFixedHeight(30f);
        cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid3.setBackgroundColor(backGris);
        tableAFE_Grid3.addCell(cellAFE_Grid3);
        
        cellAFE_Grid3 = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional",blackNegrita));
        cellAFE_Grid3.setFixedHeight(30f);
        cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellAFE_Grid3.setBackgroundColor(backGris);
        tableAFE_Grid3.addCell(cellAFE_Grid3);
        
        for(int i=0;i<arrayAcciones.length();i++){
     	   
     	   int cnt = i+1;
     	   cellAFE_Grid3 = new PdfPCell(new Paragraph(""+cnt,black));
     	   cellAFE_Grid3.setFixedHeight(40f);
     	   cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
     	   tableAFE_Grid3.addCell(cellAFE_Grid3);
     	   
 	  
     	 String nombreAFEP = arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").getString("rpNombre")+
     	 " " + arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").getString("rpPrimerAp")+
     	 " " + arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").getString("rpSegundoAp");
     	  
     	 // System.out.print("nombreAFEP: "+nombreAFEP);
     	   
     	  cellAFE_Grid3 = new PdfPCell(new Paragraph(nombreAFEP ,black));
   	      cellAFE_Grid3.setFixedHeight(40f);
   	      cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
   	      tableAFE_Grid3.addCell(cellAFE_Grid3);
   	      
   	      cellAFE_Grid3 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCargo"),black));
	      cellAFE_Grid3.setFixedHeight(40f);
	      cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
	      tableAFE_Grid3.addCell(cellAFE_Grid3);
	      
	      cellAFE_Grid3 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").get("rpTel").toString(),black));
	      cellAFE_Grid3.setFixedHeight(40f);
	      cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
	      tableAFE_Grid3.addCell(cellAFE_Grid3);
	      
	      String extEnlAfe = (!arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").has("rpExt")?"":
      		 (arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").isNull("rpExt")?"":
           		  (arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString().equals("null")?"":
           			arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString())));
	      
	      cellAFE_Grid3 = new PdfPCell(new Paragraph(extEnlAfe,black));
	      cellAFE_Grid3.setFixedHeight(40f);
	      cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
	      tableAFE_Grid3.addCell(cellAFE_Grid3);
        
	      
	      cellAFE_Grid3 = new PdfPCell(new Paragraph(arrayAcciones.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCorreo"),black));
	      cellAFE_Grid3.setFixedHeight(40f);
	      cellAFE_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
	      tableAFE_Grid3.addCell(cellAFE_Grid3);
	      
	      
        }
        
		
        float[] widthsTableAFE_Grid3 = {30f,100f,100f,80f,40f,100f};
		 Rectangle rTableAFE_Grid3 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
   
		 tableAFE_Grid3.setWidthPercentage(widthsTableAFE_Grid3, rTableAFE_Grid3);
		 tableAFE_Grid3.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
   
		 cAFE_Grid3 = new PdfPCell (tableAFE_Grid3);
		 cAFE_Grid3.setBorder(PdfPCell.BOX);
		 cAFE_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
		 cAFE_Grid3.setPadding(0); 
      
        cell = new PdfPCell(cAFE_Grid3);
	    cell.setBorder(PdfPCell.BOX);
	    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    table2.addCell(cell);
	    
        
      String leyendaAfeInfo = "Por cada Acci\u00f3n, Fondo, Estrategia o Proyecto que registr\u00f3, seleccione la informaci\u00f3n que aportar\u00e1 al SISI";
 		
		cell = new PdfPCell(new Paragraph(leyendaAfeInfo,blackNegrita));
        cell.setBorder(PdfPCell.BOX);
        cell.setFixedHeight(20f);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        table2.addCell(cell); 
        
        PdfPTable tableAFE_Grid4 = new PdfPTable(6);
        PdfPCell  cAFE_Grid4;
        PdfPCell  cellAFE_Grid4;

        cellAFE_Grid4 = new PdfPCell(new Paragraph("N\u00famero de AFEP",blackNegrita));
        cellAFE_Grid4.setFixedHeight(40f);
        cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellAFE_Grid4.setBackgroundColor(backGris);
        tableAFE_Grid4.addCell(cellAFE_Grid4);
        
        cellAFE_Grid4 = new PdfPCell(new Paragraph("Recolecci\u00f3n de CUIS",blackNegrita));
        cellAFE_Grid4.setFixedHeight(40f);
        cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellAFE_Grid4.setBackgroundColor(backGris);
        tableAFE_Grid4.addCell(cellAFE_Grid4);
        
        cellAFE_Grid4 = new PdfPCell(new Paragraph("Informaci\u00f3n socioecon\u00f3mica",blackNegrita));
        cellAFE_Grid4.setFixedHeight(40f);
        cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellAFE_Grid4.setBackgroundColor(backGris);
        tableAFE_Grid4.addCell(cellAFE_Grid4);
        
        cellAFE_Grid4 = new PdfPCell(new Paragraph("Registros Administrativos",blackNegrita));
        cellAFE_Grid4.setFixedHeight(40f);
        cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellAFE_Grid4.setBackgroundColor(backGris);
        tableAFE_Grid4.addCell(cellAFE_Grid4);
        
        cellAFE_Grid4 = new PdfPCell(new Paragraph("Padr\u00f3n de Beneficiarios",blackNegrita));
        cellAFE_Grid4.setFixedHeight(40f);
        cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellAFE_Grid4.setBackgroundColor(backGris);
        tableAFE_Grid4.addCell(cellAFE_Grid4);
        
        cellAFE_Grid4 = new PdfPCell(new Paragraph("Infraestructura Social /Domicilios geográficos /Obras FAIS /Información Georreferenciada para el Desarrollo Social.",blackNegrita));
        cellAFE_Grid4.setFixedHeight(40f);
        cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellAFE_Grid4.setBackgroundColor(backGris);
        tableAFE_Grid4.addCell(cellAFE_Grid4);
        
	     for(int i=0;i < arrayAcciones_Inf.length();i++){
	    	 
           int cnt = i+1;

	    	String recoleccion = "NO";
	    	String infoSE = "NO";
	    	String registros = "NO";
	    	String padron = "NO";
	    	String viviendas = "NO";
				
			for(int j=0;j<arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
	    		recoleccion = arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==1?"SI":"NO";
	    		if(recoleccion.equals("SI")){
	    			break;
	    		}
	    	}
			for(int j=0;j<arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
	    		infoSE = arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==2?"SI":"NO";
	    		if(infoSE.equals("SI")){
	    			break;
	    		}
	    	}
			for(int j=0;j<arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
	    		registros = arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==3?"SI":"NO";
	    		if(registros.equals("SI")){
	    			break;
	    		}
	    	}
			for(int j=0;j<arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
	    		padron = arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==4?"SI":"NO";
	    		if(padron.equals("SI")){
	    			break;
	    		}
	    	}
			for(int j=0;j<arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
	    		viviendas = arrayAcciones_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==5?"SI":"NO";
	    		if(viviendas.equals("SI")){
	    			break;
	    		}
	    	}
	    	
			cellAFE_Grid4 = new PdfPCell(new Paragraph("" + cnt,black));
			cellAFE_Grid4.setFixedHeight(20f);
			cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
			tableAFE_Grid4.addCell(cellAFE_Grid4);
           
           // --------------------------------------------------------------------------------------------------// 
           cellAFE_Grid4 = new PdfPCell(new Paragraph(recoleccion,black));
           cellAFE_Grid4.setFixedHeight(20f);
           cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
           tableAFE_Grid4.addCell(cellAFE_Grid4);
           
           cellAFE_Grid4 = new PdfPCell(new Paragraph(infoSE,black));
           cellAFE_Grid4.setFixedHeight(20f);
           cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
           tableAFE_Grid4.addCell(cellAFE_Grid4);
           
           cellAFE_Grid4 = new PdfPCell(new Paragraph(registros,black));
           cellAFE_Grid4.setFixedHeight(20f);
           cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
           tableAFE_Grid4.addCell(cellAFE_Grid4);
           
           cellAFE_Grid4 = new PdfPCell(new Paragraph(padron,black));
           cellAFE_Grid4.setFixedHeight(20f);
           cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
           tableAFE_Grid4.addCell(cellAFE_Grid4);
           
           cellAFE_Grid4 = new PdfPCell(new Paragraph(viviendas,black));
           cellAFE_Grid4.setFixedHeight(20f);
           cellAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
           tableAFE_Grid4.addCell(cellAFE_Grid4);
	     }
	    

        float[] widthsTableAFE_Grid4 = {30f,80f,90f,90f,80f,90f};
		 Rectangle rTableAFE_Grid4 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
    
		 tableAFE_Grid4.setWidthPercentage(widthsTableAFE_Grid4, rTableAFE_Grid4);
		 tableAFE_Grid4.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
    
		 cAFE_Grid4 = new PdfPCell (tableAFE_Grid4);
		 cAFE_Grid4.setBorder(PdfPCell.BOX);
		 cAFE_Grid4.setHorizontalAlignment(Element.ALIGN_CENTER);
		 cAFE_Grid4.setPadding(0); 
        
    cell = new PdfPCell(cAFE_Grid4);
	cell.setBorder(PdfPCell.BOX);
	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	table2.addCell(cell); */   
	   
        
			 /*Fin IV. Acciones , Fondos y Estrategias*/
			
            /*Inicio IV. UA sin programas sociales*/
/*		    
		    cell = new PdfPCell(new Paragraph("V. Unidades Estrat\u00e9gicas",nomCampo));
			cell.setBorder(PdfPCell.YMARK);
			cell.setBackgroundColor(myColor);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(cell);
			
			String leyenda3 = "Registre las Unidades Estrat\u00e9gicas a su cargo, que deseen intercambiar informaci\u00f3n con el SISI. \n" +
			                  "Las Unidades Estrat\u00e9gicas son aquellas \u00e1reas que NO operan programas sociales pero que coadyuvan en su implementaci\u00f3n, o " +
					          "bien, en los procesos de planeaci\u00f3n, coordinaci\u00f3n, evaluaci\u00f3n, monitoreo y/o seguimiento de sus acciones.";
  
            cell = new PdfPCell(new Paragraph(leyenda3,blackNegrita));
            cell.setBorder(PdfPCell.BOX);
            cell.setFixedHeight(50f);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table2.addCell(cell);

		         PdfPTable tableSPS_Grid = new PdfPTable(4);
		         PdfPCell cSPS_Grid;
                 PdfPCell cellSPS_Grid;

                 cellSPS_Grid = new PdfPCell(new Paragraph("N\u00famero de UE",blackNegrita));
                 cellSPS_Grid.setFixedHeight(30f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("ID UE",blackNegrita));
                 cellSPS_Grid.setFixedHeight(30f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Nombre del \u00C1rea",blackNegrita));
                 cellSPS_Grid.setFixedHeight(30f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Funci\u00f3n que desempe\u00f1a",blackNegrita));
                 cellSPS_Grid.setFixedHeight(30f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Estrategia o acciones que implementa",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Nombre del Titular de la UE",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Cargo",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Tel\u00e9fono",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Extensi\u00f3n",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 cellSPS_Grid = new PdfPCell(new Paragraph("Estatus",blackNegrita));
                 cellSPS_Grid.setFixedHeight(50f);
                 cellSPS_Grid.setBackgroundColor(backGris);
                 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableSPS_Grid.addCell(cellSPS_Grid);
                 
                 
                 for(int i=0;i<arrayUnidades.length();i++){

                	 int cnt = i+1;
                	 cellSPS_Grid = new PdfPCell(new Paragraph(""+cnt,black));
                	 cellSPS_Grid.setFixedHeight(40f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosPrograma").getString("chrClaveUaSinPro"),black));
                	 cellSPS_Grid.setFixedHeight(40f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosPrograma").getString("chrNombreUaSinPro"),black));
                	 cellSPS_Grid.setFixedHeight(40f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosPrograma").getString("chrFuncionUaSinPro"),black));
                	 cellSPS_Grid.setFixedHeight(40f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosPrograma").getString("chrEstrategiaAccionesUaSin"),black));
                	 cellSPS_Grid.setFixedHeight(50f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 String aNombreTitular = arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpNombre")+" "+
                			 arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpPrimerAp")+" "+
                             arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpSegundoAp");
                	 
                	 cellSPS_Grid = new PdfPCell(new Paragraph(aNombreTitular,black));
                	 cellSPS_Grid.setFixedHeight(50f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCargo"),black));
                	 cellSPS_Grid.setFixedHeight(50f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").get("rpTel").toString(),black));
                	 cellSPS_Grid.setFixedHeight(50f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);
                	 
                	 cellSPS_Grid = new PdfPCell(new Paragraph(
                			 (!arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").has("rpExt")?"":
                        		 (arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").isNull("rpExt")?"":
                           		  (arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString().equals("null")?"":
                           			arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString()))),black));
                			 
                     cellSPS_Grid.setFixedHeight(50f);
                     cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                     tableSPS_Grid.addCell(cellSPS_Grid);

                	 cellSPS_Grid = new PdfPCell(new Paragraph(""+arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCorreo"),black));
                	 cellSPS_Grid.setFixedHeight(50f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);
                	 
                	 cellSPS_Grid = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosPrograma").get("baja").toString().equals("1")?"Baja":"Activo",black));
                	 cellSPS_Grid.setFixedHeight(50f);
                	 cellSPS_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableSPS_Grid.addCell(cellSPS_Grid);
                 }
                 
                 float[] widthsTableSPS_Grid = {30f,47f,110f,110f};
				 Rectangle rTableSPS_Grid = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tableSPS_Grid.setWidthPercentage(widthsTableSPS_Grid, rTableSPS_Grid);
				 tableSPS_Grid.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cSPS_Grid = new PdfPCell (tableSPS_Grid);
				 cSPS_Grid.setBorder(PdfPCell.BOX);
				 cSPS_Grid.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cSPS_Grid.setPadding(0); 
            
                 
            cell = new PdfPCell(cSPS_Grid);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);
     		                   
     		
     	     
     	      String leyendaUaInfo = "Por cada Unidad Estrat\u00e9gica que registr\u00f3, seleccione la informaci\u00f3n que aportar\u00e1 al SISI";
     	 		
     			cell = new PdfPCell(new Paragraph(leyendaUaInfo,blackNegrita));
     	        cell.setBorder(PdfPCell.BOX);
     	        cell.setFixedHeight(20f);
     	        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
     	        table2.addCell(cell); 
     		
     		 PdfPTable tableSPS_Grid3 = new PdfPTable(6);
             PdfPCell  cSPS_Grid3;
             PdfPCell  cellSPS_Grid3;
             
             cellSPS_Grid3 = new PdfPCell(new Paragraph("N\u00famero de UE",blackNegrita));
             cellSPS_Grid3.setFixedHeight(30f);
             cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
             cellSPS_Grid3.setBackgroundColor(backGris);
             tableSPS_Grid3.addCell(cellSPS_Grid3);
             
             cellSPS_Grid3 = new PdfPCell(new Paragraph("Nombre de Enlace UE",blackNegrita));
             cellSPS_Grid3.setFixedHeight(30f);
             cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
             cellSPS_Grid3.setBackgroundColor(backGris);
             tableSPS_Grid3.addCell(cellSPS_Grid3);
             
             cellSPS_Grid3 = new PdfPCell(new Paragraph("Cargo",blackNegrita));
             cellSPS_Grid3.setFixedHeight(30f);
             cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
             cellSPS_Grid3.setBackgroundColor(backGris);
             tableSPS_Grid3.addCell(cellSPS_Grid3);
             
             cellSPS_Grid3 = new PdfPCell(new Paragraph("Tel\u00e9fono",blackNegrita));
             cellSPS_Grid3.setFixedHeight(30f);
             cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
             cellSPS_Grid3.setBackgroundColor(backGris);
             tableSPS_Grid3.addCell(cellSPS_Grid3);
           
             cellSPS_Grid3 = new PdfPCell(new Paragraph("Extensi\u00f3n",blackNegrita));
             cellSPS_Grid3.setFixedHeight(30f);
             cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
             cellSPS_Grid3.setBackgroundColor(backGris);
             tableSPS_Grid3.addCell(cellSPS_Grid3);
             
             cellSPS_Grid3 = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional",blackNegrita));
             cellSPS_Grid3.setFixedHeight(30f);
             cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
             cellSPS_Grid3.setBackgroundColor(backGris);
             tableSPS_Grid3.addCell(cellSPS_Grid3);
             
             for(int i=0;i<arrayUnidades.length();i++){
          	   
          	   int cnt = i+1;
          	    cellSPS_Grid3 = new PdfPCell(new Paragraph(""+cnt,black));
             	cellSPS_Grid3.setFixedHeight(40f);
          	    cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
          	  tableSPS_Grid3.addCell(cellSPS_Grid3);
          	   
          	 cellSPS_Grid3 = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpNombre"),black));
             cellSPS_Grid3.setFixedHeight(40f);
          	 cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
          	tableSPS_Grid3.addCell(cellSPS_Grid3);
        	      
        	 cellSPS_Grid3 = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCargo"),black));
        	 cellSPS_Grid3.setFixedHeight(40f);
        	 cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
        	 tableSPS_Grid3.addCell(cellSPS_Grid3);
     	      
     	     cellSPS_Grid3 = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").get("rpTel").toString(),black));
     	    cellSPS_Grid3.setFixedHeight(40f);
     	   cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
     	  tableSPS_Grid3.addCell(cellSPS_Grid3);
     	      
     	      String extEnlUa = (!arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").has("rpExt")?"":
           		 (arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").isNull("rpExt")?"":
                		  (arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString().equals("null")?"":
                			  arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").get("rpExt").toString())));
     	      
     	     cellSPS_Grid3 = new PdfPCell(new Paragraph(extEnlUa,black));
     	    cellSPS_Grid3.setFixedHeight(40f);
     	   cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
     	  tableSPS_Grid3.addCell(cellSPS_Grid3);
             
     	      
     	     cellSPS_Grid3 = new PdfPCell(new Paragraph(arrayUnidades.getJSONObject(i).getJSONObject("datosResponsable").getString("rpCorreo"),black));
     	    cellSPS_Grid3.setFixedHeight(40f);
     	   cellSPS_Grid3.setHorizontalAlignment(Element.ALIGN_LEFT);
     	  tableSPS_Grid3.addCell(cellSPS_Grid3);
     	      
     	      
             }
             
     		
             float[] widthsTableSPS_Grid3 = {30f,100f,100f,80f,40f,100f};
     		 Rectangle rTableSPS_Grid3 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
        
     		tableSPS_Grid3.setWidthPercentage(widthsTableSPS_Grid3, rTableSPS_Grid3);
     		tableSPS_Grid3.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
        
     		 cSPS_Grid3 = new PdfPCell (tableSPS_Grid3);
     		 cSPS_Grid3.setBorder(PdfPCell.BOX);
     		 cSPS_Grid3.setHorizontalAlignment(Element.ALIGN_CENTER);
     		 cSPS_Grid3.setPadding(0); 
           
             cell = new PdfPCell(cSPS_Grid3);
     	    cell.setBorder(PdfPCell.BOX);
     	    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     	    table2.addCell(cell);
     		
     		
     		
     		String leyenda4 = "Por cada Unidad Estrat\u00e9gica que registr\u00f3, seleccione la informaci\u00f3n que aportar\u00e1 al SISI.";
            
     		cell = new PdfPCell(new Paragraph(leyenda4,blackNegrita));
     		cell.setBorder(PdfPCell.BOX);
     		cell.setFixedHeight(20f);
     		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
     		table2.addCell(cell);

		         PdfPTable tableUA_Grid2 = new PdfPTable(6);
		         PdfPCell cUS_Grid2;
                 PdfPCell cellUA_Grid2;

                 cellUA_Grid2 = new PdfPCell(new Paragraph("N\u00famero de UE",blackNegrita));
                 cellUA_Grid2.setFixedHeight(40f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellUA_Grid2.setBackgroundColor(backGris);
                 tableUA_Grid2.addCell(cellUA_Grid2);
                 
                 cellUA_Grid2 = new PdfPCell(new Paragraph("Recolecci\u00f3n de CUIS",blackNegrita));
                 cellUA_Grid2.setFixedHeight(40f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellUA_Grid2.setBackgroundColor(backGris);
                 tableUA_Grid2.addCell(cellUA_Grid2);
                 
                 cellUA_Grid2 = new PdfPCell(new Paragraph("Informaci\u00f3n socioecon\u00f3mica",blackNegrita));
                 cellUA_Grid2.setFixedHeight(40f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellUA_Grid2.setBackgroundColor(backGris);
                 tableUA_Grid2.addCell(cellUA_Grid2);
                 
                 cellUA_Grid2 = new PdfPCell(new Paragraph("Registros Administrativos",blackNegrita));
                 cellUA_Grid2.setFixedHeight(40f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellUA_Grid2.setBackgroundColor(backGris);
                 tableUA_Grid2.addCell(cellUA_Grid2);
                 
                 cellUA_Grid2 = new PdfPCell(new Paragraph("Padr\u00f3n de Beneficiarios",blackNegrita));
                 cellUA_Grid2.setFixedHeight(40f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellUA_Grid2.setBackgroundColor(backGris);
                 tableUA_Grid2.addCell(cellUA_Grid2);
                 
                 cellUA_Grid2 = new PdfPCell(new Paragraph("Infraestructura Social /Domicilios geográficos /Obras FAIS /Información Georreferenciada para el Desarrollo Social.",blackNegrita));
                 cellUA_Grid2.setFixedHeight(40f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 cellUA_Grid2.setBackgroundColor(backGris);
                 tableUA_Grid2.addCell(cellUA_Grid2);
                 
     		     for(int i=0;i < arrayUnidades_Inf.length();i++){
     		    	 
                    int cnt = i+1;

     		    	String recoleccion = "NO";
    		    	String infoSE = "NO";
    		    	String registros = "NO";
    		    	String padron = "NO";
    		    	String viviendas = "NO";
    					
    				for(int j=0;j<arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		recoleccion = arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==1?"SI":"NO";
    		    		if(recoleccion.equals("SI")){
    		    			break;
    		    		}
    		    	}
    				for(int j=0;j<arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		infoSE = arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==2?"SI":"NO";
    		    		if(infoSE.equals("SI")){
    		    			break;
    		    		}
    		    	}
    				for(int j=0;j<arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		registros = arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==3?"SI":"NO";
    		    		if(registros.equals("SI")){
    		    			break;
    		    		}
    		    	}
    				for(int j=0;j<arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		padron = arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==4?"SI":"NO";
    		    		if(padron.equals("SI")){
    		    			break;
    		    		}
    		    	}
    				for(int j=0;j<arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").length();j++){
    		    		viviendas = arrayUnidades_Inf.getJSONObject(i).getJSONArray("programaInfAportaraSisis").getJSONObject(j).getJSONObject("cinformacionAportaraSisi").getInt("cscInfoAportaSisi")==5?"SI":"NO";
    		    		if(viviendas.equals("SI")){
    		    			break;
    		    		}
    		    	}
     		    	
     		    	cellUA_Grid2 = new PdfPCell(new Paragraph("" + cnt,black));
                    cellUA_Grid2.setFixedHeight(20f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableUA_Grid2.addCell(cellUA_Grid2);
                    
                    // --------------------------------------------------------------------------------------------------// 
     		    	cellUA_Grid2 = new PdfPCell(new Paragraph(recoleccion,black));
                    cellUA_Grid2.setFixedHeight(20f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableUA_Grid2.addCell(cellUA_Grid2);
                    
                    cellUA_Grid2 = new PdfPCell(new Paragraph(infoSE,black));
                    cellUA_Grid2.setFixedHeight(20f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableUA_Grid2.addCell(cellUA_Grid2);
                    
                    cellUA_Grid2 = new PdfPCell(new Paragraph(registros,black));
                    cellUA_Grid2.setFixedHeight(20f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableUA_Grid2.addCell(cellUA_Grid2);
                    
                    cellUA_Grid2 = new PdfPCell(new Paragraph(padron,black));
                    cellUA_Grid2.setFixedHeight(20f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableUA_Grid2.addCell(cellUA_Grid2);
                    
                    cellUA_Grid2 = new PdfPCell(new Paragraph(viviendas,black));
                    cellUA_Grid2.setFixedHeight(20f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableUA_Grid2.addCell(cellUA_Grid2);
    		     }
     		    
                 
                 
                 
    
                 float[] widthsTableUA_Grid2 = {30f,80f,90f,90f,80f,90f};
				 Rectangle rTableUA_Grid2 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tableUA_Grid2.setWidthPercentage(widthsTableUA_Grid2, rTableUA_Grid2);
				 tableUA_Grid2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cUS_Grid2 = new PdfPCell (tableUA_Grid2);
				 cUS_Grid2.setBorder(PdfPCell.BOX);
				 cUS_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cUS_Grid2.setPadding(0); 
                 
            cell = new PdfPCell(cUS_Grid2);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);*/            
                    
     		
			/*Fin IV. UA sin programas sociales*/
     		
            /*Inicio V. Ejecutadores de Fondos Especiales*/
		    
		    /*cell = new PdfPCell(new Paragraph("V. Ejecutores de Fondos Especiales",nomCampo));
			cell.setBorder(PdfPCell.YMARK);
			cell.setBackgroundColor(myColor);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(cell);
			
			String leyenda5 = "Registre el(los) Fondo(s) de Aportaciones de los cuales ejecuta los recursos para la implementaci\u00f3n de proyectos en su Entidad/Municipio, y que intercambiar\u00e1n informaci\u00f3n con el SISI.";
  
            cell = new PdfPCell(new Paragraph(leyenda5,blackNegrita));
            cell.setBorder(PdfPCell.BOX);
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            table2.addCell(cell);

		         PdfPTable tableEFE_Grid = new PdfPTable(6);
		         PdfPCell cEFE_Grid;
                 PdfPCell cellEFE_Grid;

                 cellEFE_Grid = new PdfPCell(new Paragraph("N\u00famero de Fondo",blackNegrita));
                 cellEFE_Grid.setFixedHeight(50f);
                 cellEFE_Grid.setBackgroundColor(backGris);
                 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid.addCell(cellEFE_Grid);
                 
                 cellEFE_Grid = new PdfPCell(new Paragraph("Fondo del cual ejerce los recursos",blackNegrita));
                 cellEFE_Grid.setFixedHeight(50f);
                 cellEFE_Grid.setBackgroundColor(backGris);
                 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid.addCell(cellEFE_Grid);
                 
                 cellEFE_Grid = new PdfPCell(new Paragraph("Nombre del responsable de la ejecuci\u00f3n del fonde en la Entidad o Municipio",black));
                 cellEFE_Grid.setFixedHeight(50f);
                 cellEFE_Grid.setBackgroundColor(backGris);
                 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid.addCell(cellEFE_Grid);
                 
                 cellEFE_Grid = new PdfPCell(new Paragraph("Cargo",blackNegrita));
                 cellEFE_Grid.setFixedHeight(50f);
                 cellEFE_Grid.setBackgroundColor(backGris);
                 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid.addCell(cellEFE_Grid);
                 
                 cellEFE_Grid = new PdfPCell(new Paragraph("Tel\u00e9fono/Extensi\u00f3n",blackNegrita));
                 cellEFE_Grid.setFixedHeight(50f);
                 cellEFE_Grid.setBackgroundColor(backGris);
                 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid.addCell(cellEFE_Grid);
                 
                 cellEFE_Grid = new PdfPCell(new Paragraph("Correo electr\u00f3nico institucional",blackNegrita));
                 cellEFE_Grid.setFixedHeight(50f);
                 cellEFE_Grid.setBackgroundColor(backGris);
                 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid.addCell(cellEFE_Grid);
                 
                 JSONArray arrayFe = jsonObj.getJSONArray("tablaArrayFe");
                 
                 for(int i=0;i<arrayFe.length();i++){

                	 int cnt = i+1;
                	 cellEFE_Grid = new PdfPCell(new Paragraph(""+cnt,black));
                	 cellEFE_Grid.setFixedHeight(50f);
                	 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableEFE_Grid.addCell(cellEFE_Grid);

                	 cellEFE_Grid = new PdfPCell(new Paragraph(""+arrayFe.getJSONObject(i).getJSONObject("cdescripcionFondo").getInt("cscDescripcionFondo"),black));
                	 cellEFE_Grid.setFixedHeight(50f);
                	 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableEFE_Grid.addCell(cellEFE_Grid);
                	 
                	 String aNombreTitular = arrayUaSn.getJSONObject(i).getString("rpNombre")+
                			 arrayUaSn.getJSONObject(i).getString("rpPrimerAp")+
                             arrayUaSn.getJSONObject(i).getString("rpSegundoAp");

                	 cellEFE_Grid = new PdfPCell(new Paragraph(aNombreTitular,black));
                	 cellEFE_Grid.setFixedHeight(50f);
                	 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableEFE_Grid.addCell(cellEFE_Grid);

                	 cellEFE_Grid = new PdfPCell(new Paragraph(arrayFe.getJSONObject(i).getString("rpCargo"),black));
                	 cellEFE_Grid.setFixedHeight(50f);
                	 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableEFE_Grid.addCell(cellEFE_Grid);
                	 
                	 cellEFE_Grid = new PdfPCell(new Paragraph(arrayFe.getJSONObject(i).get("rpTel").toString()+"/"+arrayFe.getJSONObject(i).get("rpExt").toString(),black));
                	 cellEFE_Grid.setFixedHeight(50f);
                	 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableEFE_Grid.addCell(cellEFE_Grid);
                	 
                	 cellEFE_Grid = new PdfPCell(new Paragraph(arrayFe.getJSONObject(i).getString("rpCorreo"),black));
                	 cellEFE_Grid.setFixedHeight(50f);
                	 cellEFE_Grid.setHorizontalAlignment(Element.ALIGN_LEFT);
                	 tableEFE_Grid.addCell(cellEFE_Grid);

                 }
                 
                 float[] widthsTableEFE_Grid = {30f,120f,120f,50f,50f,100f};
				 Rectangle rTableEFE_Grid = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tableEFE_Grid.setWidthPercentage(widthsTableEFE_Grid, rTableEFE_Grid);
				 tableEFE_Grid.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cEFE_Grid = new PdfPCell (tableEFE_Grid);
				 cEFE_Grid.setBorder(PdfPCell.BOX);
				 cEFE_Grid.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cEFE_Grid.setPadding(0); 
            
                 
            cell = new PdfPCell(cEFE_Grid);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);
     		
     		String leyenda6 = "Registre las consultas de productos y servicios requeridos y en su caso, la informaci\u00f3n que aportar\u00e1 al SISI. El registro debe ser por cada Fondo conforme a lo"+
     		                  "se\u00e1alado en el apartado anterior. \n"+ 
     				          "La informaci\u00f3n que proporcione en este apartado, definir\u00e1 el curso del intercambio de informaci\u00f3n, la cual ser\u00e1 aplicable durante el ejercicio fiscal vigente y se "+
     		                  "actualizar\u00e1 anualmente. En caso de requerir alguna modificaci\u00f3n antes de la actualizaci\u00f3n, se podr\u00e1n sumar requerimientos pero no podr\u00e1n eliminarse.";
            
     		cell = new PdfPCell(new Paragraph(leyenda6,blackNegrita));
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
     		table2.addCell(cell);

     		     PdfPTable tableEFE_Grid2Header = new PdfPTable(3);
	             PdfPCell cEFE_Grid2Header;
                 PdfPCell cellEFE_Grid2Header;
                 
                 cellEFE_Grid2Header = new PdfPCell(new Paragraph("",blackNegrita));
                 cellEFE_Grid2Header.setFixedHeight(15f);
                 cellEFE_Grid2Header.setBackgroundColor(backGris);
                 cellEFE_Grid2Header.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2Header.addCell(cellEFE_Grid2Header);
                 
                 cellEFE_Grid2Header = new PdfPCell(new Paragraph("Consulta de Productos/Servicios del SISI",blackNegrita));
                 cellEFE_Grid2Header.setFixedHeight(15f);
                 cellEFE_Grid2Header.setBackgroundColor(backGris);
                 cellEFE_Grid2Header.setHorizontalAlignment(Element.ALIGN_CENTER);
                 tableEFE_Grid2Header.addCell(cellEFE_Grid2Header);
                 
                 cellEFE_Grid2Header = new PdfPCell(new Paragraph("Informaci\u00f3n que aportar\u00e1 al SISI",blackNegrita));
                 cellEFE_Grid2Header.setFixedHeight(15f);
                 cellEFE_Grid2Header.setBackgroundColor(backGris);
                 cellEFE_Grid2Header.setHorizontalAlignment(Element.ALIGN_CENTER);
                 tableEFE_Grid2Header.addCell(cellEFE_Grid2Header);
                 
                 float[] widthsTableEFE_Grid2Header = {30f,240f,200f};
				 Rectangle rTableEFE_Grid2Header = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tableEFE_Grid2Header.setWidthPercentage(widthsTableEFE_Grid2Header, rTableEFE_Grid2Header);
				 tableEFE_Grid2Header.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cEFE_Grid2Header = new PdfPCell (tableEFE_Grid2Header);
				 cEFE_Grid2Header.setBorder(PdfPCell.BOX);
				 cEFE_Grid2Header.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cEFE_Grid2Header.setPadding(0); 
            
                 
            cell = new PdfPCell(cEFE_Grid2Header);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);
     		
		         PdfPTable tableEFE_Grid2 = new PdfPTable(12);
		         PdfPCell cEFE_Grid2;
                 PdfPCell cellEFE_Grid2;

                 cellEFE_Grid2 = new PdfPCell(new Paragraph("N\u00famero de Fondo",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Universos Potenciales",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Informaci\u00f3n socioecon\u00f3mica",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Cobertura agregada",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Confronta nominal de beneficiarios",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Capas de informaci\u00f3n geoespacial",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Mapas tem\u00e1ticos",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Recolecci\u00f3n de CUIS",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Informaci\u00f3n socioecon\u00f3mica de Encuesta propia",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Registros Administrativos",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellEFE_Grid2 = new PdfPCell(new Paragraph("Padr\u00f3n de Beneficiarios",blackNegrita));
                 cellEFE_Grid2.setFixedHeight(50f);
                 cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellEFE_Grid2);
                 
                 cellUA_Grid2 = new PdfPCell(new Paragraph("Viviendas/Obras/Infraestructura \nGeoreferenciada/capas geograficas",blackNegrita));
                 cellUA_Grid2.setFixedHeight(50f);
                 cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_LEFT);
                 tableEFE_Grid2.addCell(cellUA_Grid2);
                 
                 JSONArray arrayFe_PS = jsonObj.getJSONArray("tablaArrayFePs");
                 
                 JSONArray arrayFeInf = jsonObj.getJSONArray("tablaArrayFeInf");
     		    
     		     for(int i=0;i < arrayFe_PS.length();i++){
     		    	 
     		    	 int cnt = i+1;
     		    	 
     		    	 String universos = "NO";
     		    	 String infoSo = "NO";
     		    	 String cobertura = "NO";
     		    	 String confronta = "NO";
     		    	 String capas = "NO";
     		    	 String mapas = "NO";
     		    	 
     		    	 System.out.println(arrayFe_PS.getJSONArray(i));
     		    	 
     		    	 for(int j=0;j<arrayFe_PS.getJSONArray(i).length();j++){
     		    		universos = (arrayFe_PS.getJSONArray(i).getJSONObject(j).has("universos"))?"SI":"NO";
     		    		if(universos.equals("SI")){
     		    			break;
     		    		}
     		    	 }
     		    	 for(int j=0;j<arrayFe_PS.getJSONArray(i).length();j++){
     		    		infoSo = (arrayFe_PS.getJSONArray(i).getJSONObject(j).has("infoSo"))?"SI":"NO";
     		    		if(infoSo.equals("SI")){
     		    			break;
     		    		}
     		    	 }
     		    	for(int j=0;j<arrayFe_PS.getJSONArray(i).length();j++){
     		    		cobertura = (arrayFe_PS.getJSONArray(i).getJSONObject(j).has("cobertura"))?"SI":"NO";
     		    		if(cobertura.equals("SI")){
     		    			break;
     		    		}
     		    	}
     		    	for(int j=0;j<arrayFe_PS.getJSONArray(i).length();j++){
     		    		confronta = (arrayFe_PS.getJSONArray(i).getJSONObject(j).has("confronta"))?"SI":"NO";
     		    		if(confronta.equals("SI")){
     		    			break;
     		    		}
     		    	}
     		    	for(int j=0;j<arrayFe_PS.getJSONArray(i).length();j++){
     		    		capas = (arrayFe_PS.getJSONArray(i).getJSONObject(j).has("capas"))?"SI":"NO";
     		    		if(capas.equals("SI")){
     		    			break;
     		    		}
     		    	}
     		    	for(int j=0;j<arrayFe_PS.getJSONArray(i).length();j++){
     		    		mapas = (arrayFe_PS.getJSONArray(i).getJSONObject(j).has("mapas"))?"SI":"NO";
     		    		if(mapas.equals("SI")){
     		    			break;
     		    		}
     		    	}

     		    	String recoleccion = "NO";
    		    	String infoSE = "NO";
    		    	String registros = "NO";
    		    	String padron = "NO";
    		    	String viviendas = "NO";
    		    	 
    		    	for(int j=0;j<arrayFeInf.getJSONArray(i).length();j++){
    		    		recoleccion = (arrayFeInf.getJSONArray(i).getJSONObject(j).has("recoleccion"))?"SI":"NO";
    		    		if(recoleccion.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayFeInf.getJSONArray(i).length();j++){
    		    		infoSE = (arrayFeInf.getJSONArray(i).getJSONObject(j).has("infoSE"))?"SI":"NO";
    		    		if(infoSE.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayFeInf.getJSONArray(i).length();j++){
    		    		registros = (arrayFeInf.getJSONArray(i).getJSONObject(j).has("registros"))?"SI":"NO";
    		    		if(registros.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayFeInf.getJSONArray(i).length();j++){
    		    		padron = (arrayFeInf.getJSONArray(i).getJSONObject(j).has("padron"))?"SI":"NO";
    		    		if(padron.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	for(int j=0;j<arrayFeInf.getJSONArray(i).length();j++){
    		    		viviendas = (arrayFeInf.getJSONArray(i).getJSONObject(j).has("viviendas"))?"SI":"NO";
    		    		if(viviendas.equals("SI")){
    		    			break;
    		    		}
    		    	}
    		    	
    		    	cellEFE_Grid2 = new PdfPCell(new Paragraph(""+cnt,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(universos,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(infoSo,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(cobertura,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(confronta,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(capas,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(mapas,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(recoleccion,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(infoSE,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(registros,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellEFE_Grid2 = new PdfPCell(new Paragraph(padron,black));
                    cellEFE_Grid2.setFixedHeight(50f);
                    cellEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellEFE_Grid2);
                    
                    cellUA_Grid2 = new PdfPCell(new Paragraph(viviendas,black));
                    cellUA_Grid2.setFixedHeight(50f);
                    cellUA_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
                    tableEFE_Grid2.addCell(cellUA_Grid2);
    		    	
     		     }
    
                 float[] widthsTableEFE_Grid2 = {30f,40f,40f,40f,40f,40f,40f,40f,40f,40f,40f,40f};
				 Rectangle rTableEFE_Grid2 = new Rectangle(PageSize.A4.getRight(150), PageSize.A4.getTop(120));
		     
				 tableEFE_Grid2.setWidthPercentage(widthsTableEFE_Grid2, rTableEFE_Grid2);
				 tableEFE_Grid2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
		     
				 cEFE_Grid2 = new PdfPCell (tableEFE_Grid2);
				 cEFE_Grid2.setBorder(PdfPCell.BOX);
				 cEFE_Grid2.setHorizontalAlignment(Element.ALIGN_CENTER);
				 cEFE_Grid2.setPadding(0); 
                 
            cell = new PdfPCell(cEFE_Grid2);
     		cell.setBorder(PdfPCell.BOX);
     		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
     		table2.addCell(cell);            
              */      
     		
		 
            
          
     		
     		
            /*Fin Autorizacion*/
			
			table2.setWidthPercentage(widths1, r1);
			table2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
			document.add(table2);
			
			
			
		} catch (Exception de) {
			de.printStackTrace();
		}
		
		document.close();
		
		/*File file = new File("/home/lgskam/Descargas/prueba.pdf");
		try {
			Desktop.getDesktop().open(file);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return response;
	}

	class HeaderFooter extends PdfPageEventHelper { 

		Phrase piePagina = new Phrase();
		Phrase pieLeyenda = new Phrase();
		@Override
		public void onEndPage(PdfWriter writer, Document document) {

			PdfContentByte cb = writer.getDirectContent();

			piePagina = new Phrase(String.format("BE-230 (042018)"), FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, new BaseColor(0, 0, 0)));
			ColumnText.showTextAligned(cb,Element.ALIGN_RIGHT, piePagina, 100,20,0);
			
			//piePagina = new Phrase(String.format("Direcci\u00f3n General de Geoestad\u00edstica y Padrones de Beneficiarios"), FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, new BaseColor(0, 0, 0)));
			//ColumnText.showTextAligned(cb,Element.ALIGN_RIGHT, piePagina, 264,25,0);

			piePagina = new Phrase(String.format("Pagina %d",writer.getCurrentPageNumber()), FontFactory.getFont(FontFactory.HELVETICA, 8, Font.BOLD, new BaseColor(0, 0, 0)));
			ColumnText.showTextAligned(cb,Element.ALIGN_RIGHT, piePagina, 600,20,0);	      
		}   
	}
}
