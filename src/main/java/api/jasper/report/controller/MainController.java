package api.jasper.report.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import api.jasper.report.build.ReportRegistroPDF;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Controller
public class MainController {
	
	

	 
	 @RequestMapping(value = "/api/report/be-001", method = {RequestMethod.GET,RequestMethod.POST}, produces="application/json")
		@ResponseBody
		public void exportarConvenioPDF(HttpServletRequest request,HttpServletResponse response,InputStream incomingData) throws UnsupportedEncodingException {
			String jsonReturn = "";
			StringBuilder builder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData, "UTF-8"));             
			String line = null; 

			
			

			try{

				while ((line = in.readLine()) != null) {                 
					builder.append(line);             
				}

				String jsonData = builder.toString();
				//
				
				response.setContentType("application/pdf");
				response.setCharacterEncoding("utf-8");

				ReportRegistroPDF report = new ReportRegistroPDF();

				JSONObject xmlJSONObj = new JSONObject(jsonData);

				response = report.creandoPDF(xmlJSONObj,response);
				
				

				response.addHeader("Content-Disposition", "attachment;filename=\"CONVENIO_Prueba.pdf\"" );

			}catch(Exception e){
				e.printStackTrace();
			}
		}
	 

}
